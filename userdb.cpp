#include "userdb.h"
#include "utc_time.h"
#include <string>
#include <stdexcept>
#include <set>
#include <iostream>
#include <filesystem>

int64_t simpleHash(const std::string value)
{
    int64_t returnValue = 0;
    short pos = 0;
    for (size_t i = 0; i < value.length(); i++) {
        returnValue ^= value[i]<<pos;
        if(pos < 57)
            pos++;
        else
            pos = 0;
    }
    return returnValue;
}


std::string decode(const std::string &password, const Blob &blob)
{
    std::string str;
    str.reserve(blob.getSize()+1);
    size_t max = blob.getSize();
    size_t passwdSize = password.length();
    if(passwdSize  < 1)
    {
        throw std::runtime_error("Password too short!");
    }
    size_t passwdIterator = 0;
    char* ptr = (char *) blob.getPtr();
    for(size_t i = 0; i < max; i++)
    {
        str += ptr[i] ^ password[passwdIterator];
        if(passwdIterator < passwdSize)
        {
            passwdIterator++;
        }
        else
        {
            passwdIterator = 0;
        }
    }
    return str;
}

Blob encode(const std::string &password, const std::string &text)
{
    char *data = new char[text.length()]();
    size_t max = text.length();
    size_t passwdIterator = 0;
    size_t passwdSize = password.length();
    if(passwdSize  < 1)
    {
        throw std::runtime_error("Password too short!");
    }
    for(size_t i = 0; i < max; i++)
    {
        data[i] = text[i] ^ password[passwdIterator];
        if(passwdIterator < passwdSize)
        {
            passwdIterator++;
        }
        else
        {
            passwdIterator = 0;
        }
    }
    Blob blob(data, max);
    return blob;
}



userDB::userDB()
{

}


void userDB::open(const std::string &filename)
{
	std::filesystem::path path(filename);
    if(std::filesystem::exists(path))
    {
        connection.open(filename);
    }
	else
	{
		std::cout << "Could not open " << filename << std::endl;
	}
//    connection.open(filename);
}

void userDB::close()
{
    connection.close();
}

void userDB::initializeDatabase(const std::string &password)
{
    int64_t hash = simpleHash(password);
    {
        Statement s = connection.prepareStatement(
                    "CREATE TABLE borrowers(id INTEGER PRIMARY KEY, barcode TEXT UNIQUE, firstname BLOB, lastname BLOB);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE TABLE tags_relations(user_id INTEGER, tag_id INTEGER);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE TABLE tags(tag_id INTEGER PRIMARY KEY, tag_name TEXT);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE TABLE borrowings(user_barcode TEXT, isbn TEXT, date_time_utc_posix INTEGER, extra_periods INTEGER);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE TABLE password(hash INTEGER);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE INDEX barcode_index ON borrowers(barcode);");
        s.step();
        s.finalize();
    }
    {
        Statement s = connection.prepareStatement(
                    "CREATE INDEX tag_index ON tags(tag_name);");
        s.step();
        s.finalize();
    }
    Statement insert_passwd = connection.prepareStatement(
                            std::string(
                                "INSERT INTO password(hash) VALUES(?);"
                            ).c_str()
                );
    insert_passwd.bind(1, hash);
    insert_passwd.step();
    insert_passwd.finalize();
}

std::list<std::string> userDB::getTagsForUser(const User &user) const
{
    std::list<std::string> returnValue;

    int64_t id = 0;
    Statement getId = connection.prepareStatement(
                "SELECT id FROM borrowers WHERE barcode = ?;");
    getId.bind(1, user.barcode);
    if(getId.step())
    {
        id = getId.getInt64(0);
    }
    else
    {
        throw std::logic_error("userDB::getTagsForUser: step() didn't return a value.");
    }

    Statement s = connection.prepareStatement(
                "SELECT tag_name FROM tags JOIN tags_relations ON tags.tag_id = tags_relations.tag_id WHERE tags_relations.user_id = ?;");
    s.bind(1, id);
    while(s.step())
    {
        returnValue.push_back(s.getText(0));
    }

    return returnValue;
}

User userDB::getUserByBarcode(const std::string &barcode, const std::string &password) const
{
    User u;
    u.barcode = "";
    u.lastname = "";
    u.firstname = "";

    Statement s = connection.prepareStatement(
                "SELECT barcode, lastname, firstname FROM borrowers WHERE barcode = ?;");
    s.bind(1, barcode);
    if(s.step())
    {
        u.barcode = s.getText(0);
        u.lastname = decode(password, s.getBlob(1));
        u.firstname = decode(password, s.getBlob(2));
    }
    else
    {
        throw std::runtime_error("userDB::getUserByBarcode: Barcode not found.");
    }
    if(s.step())
    {
        throw std::runtime_error("userDB::getUserByBarcode: step() brought another row! User-Barcode not unique?");
    }
    s.finalize();
    return u;
}

void userDB::updateUser(const User &user, const std::string &password)
{
    Statement s = connection.prepareStatement(
                "SELECT id FROM borrowers WHERE barcode = ?;");
    s.bind(1, user.barcode);
    if(s.step())
    {
        int64_t id = s.getInt64(0);
        Statement s2 = connection.prepareStatement(
                    "UPDATE borrowers SET lastname = ?, firstname = ? WHERE id = ?");
        Blob lastname_blob(encode(password, user.lastname));
        Blob firstname_blob(encode(password, user.firstname));
        s2.bind(1, lastname_blob);
        s2.bind(2, firstname_blob);
        s2.bind(3, id);
        s2.step();
        s2.finalize();
    }
    else
    {
        throw std::runtime_error("userDB::getUserByBarcode: Barcode not found.");
    }
    if(s.step())
    {
        throw std::runtime_error("userDB::getUserByBarcode: step() brought another row! User-Barcode not unique?");
    }
    s.finalize();
}

std::string userDB::addUser(const User &user, const std::string &password)
{
    int64_t newId = 0;
    Statement getMaxId_stmt = connection.prepareStatement("SELECT MAX(id) FROM borrowers;");
    if(getMaxId_stmt.step())
    {
        newId = getMaxId_stmt.getInt64(0) + 1;
    }
    getMaxId_stmt.finalize();

    char * id_str = new char[16]();
    snprintf(id_str, 16, "%08ld", newId);
    std::string idString(id_str);

    Statement s = connection.prepareStatement(
                "INSERT INTO borrowers (id, barcode, lastname, firstname) VALUES (?, ?, ?, ?)");
    Blob lastname_blob(encode(password, user.lastname));
    Blob firstname_blob(encode(password, user.firstname));
    s.bind(1, newId);
    s.bind(2, idString);
    s.bind(3, lastname_blob);
    s.bind(4, firstname_blob);
    s.step();
    s.finalize();
    return idString;
}

void userDB::setTagsForUser(const std::list<std::string> &tags, const User &user)
{
    std::list<std::string> current_tags = getTagsForUser(user);
    std::list<std::string> tags_to_delete;
    for(auto compare1 = current_tags.begin(); compare1 != current_tags.end(); compare1++)
    {
        bool to_keep = false;
        for(auto compare2 = tags.begin(); compare2 != tags.end(); compare2++)
        {
            if(*compare1 == *compare2)
            {
                to_keep = true;
            }
        }
        if(!to_keep)
            tags_to_delete.push_back(*compare1);
    }

    Statement s = connection.prepareStatement(
                "SELECT id FROM borrowers WHERE barcode = ?;");
    s.bind(1, user.barcode);

    if(s.step())
    {
        int64_t id = s.getInt64(0);

        for(std::string tag : tags)
        {
            int64_t tag_id = 0;
            Statement s2 = connection.prepareStatement(
                        "SELECT tag_id FROM tags WHERE tag_name = ?;");
            s2.bind(1, tag);
            if(s2.step())
            {
                tag_id = s2.getInt64(0);
            }
            else // add tag to database
            {
                Statement getMaxIdOfTags = connection.prepareStatement(
                            "SELECT MAX(tag_id) FROM tags;");
                if(getMaxIdOfTags.step())
                {
                    tag_id = getMaxIdOfTags.getInt64(0) + 1;
                }
                else
                {
                    throw std::runtime_error("userDB::setTagsForUser: step() didn't return a row.");
                }
                getMaxIdOfTags.finalize();
                Statement insertTag = connection.prepareStatement(
                            "INSERT INTO tags (tag_id, tag_name) VALUES (?, ?);");
                insertTag.bind(1, tag_id);
                insertTag.bind(2, tag);
                insertTag.step();
                insertTag.finalize();
            }
            s2.finalize();

            // establish relation of id/user_id and tag_id ind tag_relations if not exists
            Statement relation_find = connection.prepareStatement(
                        "SELECT tag_id, user_id FROM tags_relations WHERE tag_id = ? AND user_id = ?;");
            relation_find.bind(1, tag_id);
            relation_find.bind(2, id);
            if(!relation_find.step())
            { // add relation
                Statement relation_add = connection.prepareStatement(
                            "INSERT INTO tags_relations(user_id, tag_id) VALUES (?, ?)");
                relation_add.bind(1, id);
                relation_add.bind(2, tag_id);
                relation_add.step();
                relation_add.finalize();
            }
            relation_find.finalize();
        }

        // remove tag-relations from tags_to_delete
        for(std::string to_delete : tags_to_delete)
        {
            Statement getTagId = connection.prepareStatement("SELECT tag_id FROM tags WHERE tag_name = ?;");
            getTagId.bind(1, to_delete);
            if(getTagId.step())
            {
                int64_t tag_id = getTagId.getInt64(0);

                Statement s = connection.prepareStatement(
                            "DELETE FROM tags_relations WHERE user_id = ? AND tag_id = ?;");
                s.bind(1, id);
                s.bind(2, tag_id);
                s.step();
                s.finalize();

                Statement countRelationsForTagId = connection.prepareStatement(
                            "SELECT COUNT(user_id) FROM tags_relations WHERE tag_id = ?;");
                countRelationsForTagId.bind(1, tag_id);
                if(countRelationsForTagId.step())
                {
                    int64_t remaining = countRelationsForTagId.getInt64(0);
                    if(remaining == 0)
                    {
                        Statement s = connection.prepareStatement(
                                    "DELETE FROM tags WHERE tag_id = ?;");
                        s.bind(1, tag_id);
                        s.step();
                        s.finalize();
                    }
                }
                else
                {
                    throw std::runtime_error("userDB::setTagsForUser: step() didn't bring a row for a COUNT() statement.");
                }
                countRelationsForTagId.finalize();
            }
            else
            {
                throw std::runtime_error("userDB::setTagsForUser: step() didn't bring a row.");
            }
            getTagId.finalize();
        }
    }
    else
    {
        throw std::runtime_error("userDB::setTagsForUser: Barcode not found.");
    }
    if(s.step())
    {
        throw std::runtime_error("userDB::setTagsForUser: step() brought another row! User-Barcode not unique?");
    }
    s.finalize();
}

bool userDB::isValidDB()
{
    std::set<std::string> tables;
    tables.insert("borrowers");
    tables.insert("tags_relations");
    tables.insert("tags");
    tables.insert("borrowings");
    tables.insert("password");
    Statement s = connection.prepareStatement("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';");
    while(s.step())
    {
        std::string table_name = s.getText(0);
        if(tables.find(table_name) != tables.end())
        {
            tables.erase(table_name);
        }
    }
    if(tables.empty())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool userDB::passwordValid(const std::string &password)
{
    if(connection.getIsOpen() && isValidDB())
    {
        Statement s = connection.prepareStatement("SELECT hash FROM password;");
        if(s.step())
        {
            if(s.getInt64(0) == simpleHash(password))
            {
                return true;
            }
        }
    }
    return false;
}


std::list<std::list<std::string>> maximizeSearchterms(const std::list<std::string> &searchterms)
{
    std::list<std::list<std::string>> interpretations;

    if(searchterms.size() > 32 || searchterms.size() == 0)
    {
        throw std::runtime_error("Not enough or too many searchterms.");
    }

    uint32_t bitmask = 0xffffffff;
    bitmask >>= 32-(searchterms.size());

    for(size_t i = 0; i <= bitmask; i++)
    {
        uint32_t b = i;
        std::list<std::string> newList;
        std::string to_append = *(searchterms.cbegin());
        for(
            std::list<std::string>::const_iterator iter = ++searchterms.cbegin();
            iter != searchterms.cend();
            iter++)
        {
            if((b&1) == 1)
            {
                to_append += " " + *iter;
            }
            else
            {
                newList.push_back(to_append);
                to_append = *iter;
            }
            b >>= 1;
        }
        newList.push_back(to_append);
        interpretations.push_back(newList);
    }
    return interpretations;
}

std::list<User> userDB::getUsersForSearchstring(const std::string & password, const std::string &str) const
{
    size_t iter = 0;
    std::list<std::string> searchterms;
    std::list<std::list<std::string>> searchtermList;
    std::list<std::list<std::string>> reducedSearchtermList;

    std::list<User> result;
    if(str.empty())
    {
        return result;
    }
    while(iter < str.length() && iter != std::string::npos)
    {
        size_t iter_old = iter;
        iter = str.find(' ', iter);
        std::string sub = str.substr(iter_old, iter-iter_old);
        if(iter != std::string::npos)
            iter++; // skip whitespace
        if(!sub.empty())
        {
            searchterms.push_back(sub);
        }
    }


    // create more searchterms by joining adjacent terms with whitespaces.
    searchtermList = maximizeSearchterms(searchterms);

    // Flaw: possibility to search many tags needed.
    // Was: reduce to those fitting into the search (can be firstname, lastname or tag)
    // so reduce to all of size equal or below 3
    //
    for(std::list<std::string> iter : searchtermList)
    {
        if(/*iter.size() < 4*/ true) // was a erronous optimisation.
        {
            reducedSearchtermList.push_back(iter);
        }
    }

    std::string searchString(
                "SELECT barcode, firstname, lastname FROM borrowers WHERE"); // tags_relations, tags

    for(std::list<std::string> stlist : reducedSearchtermList)
    {
        searchString += "(";
        for(std::string st : stlist)
        {
            searchString += " (borrowers.firstname = ? OR borrowers.lastname = ? OR 0 < (SELECT COUNT(tags.tag_name) FROM tags_relations, tags WHERE tags.tag_name = ? AND (tags.tag_id = tags_relations.tag_id) AND (tags_relations.user_id = borrowers.id))) AND";
        }
        searchString += " TRUE) OR";
    }
    searchString += " FALSE;";

    int i = 1;
    Statement s = connection.prepareStatement(searchString);

    std::list<Blob> blob_list;
    for(std::list<std::string> stlist : reducedSearchtermList)
    {
        for(std::string & st : stlist)
        {
            blob_list.push_back(encode(password, st));
            s.bind(i, blob_list.back());
            i++;
            s.bind(i, blob_list.back());
            i++;
            s.bind(i, st);
            i++;
        }
    }
    while(s.step())
    {
        User add;
        add.barcode = s.getText(0);
        add.firstname = decode(password, s.getBlob(1));
        add.lastname = decode(password, s.getBlob(2));
        result.push_back(add);
    }
    s.finalize();
    return result;
}


std::list<Borrowing> userDB::getBorrowingsForUser(const User &user) const
{
    std::list<Borrowing> borrowings;

    Statement s = connection.prepareStatement(
                "SELECT isbn, date_time_utc_posix, extra_periods FROM borrowings WHERE user_barcode = ?;");
    s.bind(1, user.barcode);
    while(s.step())
    {
        Borrowing b;
        b.isbn = s.getText(0);
        b.date_time_utc_posix = s.getInt64(1);
        b.extra_periods = s.getInt64(2);
        b.user_barcode = user.barcode;
        borrowings.push_back(b);
    }
    s.finalize();

    return borrowings;
}

std::list<Borrowing> userDB::getBorrowingsForISBN(const std::string &isbn)
{
    std::list<Borrowing> borrowings;

    Statement s = connection.prepareStatement(
                "SELECT user_barcode, date_time_utc_posix, extra_periods FROM borrowings WHERE isbn = ?;");
    s.bind(1, isbn);
    while(s.step())
    {
        Borrowing b;
        b.isbn = isbn;
        b.date_time_utc_posix = s.getInt64(1);
        b.extra_periods = s.getInt64(2);
        b.user_barcode = s.getText(0);
        borrowings.push_back(b);
    }
    s.finalize();

    return borrowings;
}

std::list<Borrowing> userDB::getAllBorrowings()
{
    std::list<Borrowing> borrowings;

    Statement s = connection.prepareStatement(
                "SELECT user_barcode, date_time_utc_posix, extra_periods, isbn FROM borrowings;");
    while(s.step())
    {
        Borrowing b;
        b.isbn = s.getText(3);
        b.date_time_utc_posix = s.getInt64(1);
        b.extra_periods = s.getInt64(2);
        b.user_barcode = s.getText(0);
        borrowings.push_back(b);
    }
    s.finalize();

    return borrowings;
}

void userDB::borrowBook(const User &user, const std::string isbn)
{
    Statement s = connection.prepareStatement(
                "INSERT INTO borrowings(user_barcode, isbn, date_time_utc_posix, extra_periods) VALUES (?, ?, ?, ?)");
    time_t time = get_current_utc();
    int64_t extra_periods = 0;
    s.bind(1, user.barcode);
    s.bind(2, isbn);
    s.bind(3, time);
    s.bind(4, extra_periods);
    s.step();
    s.finalize();
}

void userDB::returnBook(const Borrowing &to_return)
{
    Statement s = connection.prepareStatement(
                "DELETE FROM borrowings WHERE user_barcode = ? AND isbn = ? AND date_time_utc_posix =? AND extra_periods = ?;");
    s.bind(1, to_return.user_barcode);
    s.bind(2, to_return.isbn);
    s.bind(3, to_return.date_time_utc_posix);
    s.bind(4, to_return.extra_periods);
    s.step();
    s.finalize();
}

void userDB::incrementExtraPeriods(Borrowing b)
{
    Statement s = connection.prepareStatement(
                "UPDATE borrowings SET extra_periods = extra_periods+1 WHERE user_barcode = ? AND isbn = ? AND date_time_utc_posix = ?;");
    s.bind(1, b.user_barcode);
    s.bind(2, b.isbn);
    s.bind(3, b.date_time_utc_posix);
    s.step();
    s.finalize();
}

void userDB::removeUser(const User &user)
{
    std::list<std::string> empty_taglist;
    setTagsForUser(empty_taglist, user);
    Statement s = connection.prepareStatement(
                "DELETE FROM borrowers WHERE barcode = ?;");
    s.bind(1, user.barcode);
    s.step();
    s.finalize();
}
