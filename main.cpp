#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <cctype>

#include <curl/curl.h>

#include "userdb.h"
#include "lookupbook.h"
#include "bookcache.h"
#include "utc_time.h"

userDB uDB;
BookCache bookcache;
std::string password;

bool is_numeric(std::string & str)
{
	int str_len = str.length();
	for(int i = 0; i < str_len; i++)
	{
		if(!std::isdigit(str[i]))
			return false;
	}
	return true;
}

bool contains_digit(std::string & str)
{
	int str_len = str.length();
	for(int i = 0; i < str_len; i++)
	{
		if(std::isdigit(str[i]))
			return true;
	}
	return false;
}

void print_user(User &user)
{
	std::list<std::string> tags;
	try {
		tags = uDB.getTagsForUser(user);
	}	
	catch(std::runtime_error e)
	{
		std::cout << e.what() << std::endl;
		goto failed;
	}
	std::cout << "\nTags:";
	for(auto & t : tags)
	{
		std::cout << " " << t ;
	}
	std::cout << std::endl;
	std::cout << "Vorname: " << user.firstname << std::endl;
	std::cout << "Nachname: " << user.lastname << std::endl;
	failed:
	;
}

User choose_user()
{
	User user;
	std::string eingabe;
	std::cout
		<< "Bitte Bibliotheksausweis einlesen oder Namen"
		" eingeben und mit 'Enter' bestätigen."
		<< std::endl;
	std::getline(std::cin, eingabe);
	if(is_numeric(eingabe))
	{
		std::cout << "Barcode eingelesen." << std::endl;
		try {
			user = uDB.getUserByBarcode(eingabe, password);
		}
		catch(std::runtime_error e)
		{
			std::cout << e.what() << std::endl;
			goto failed;
		}
		print_user(user);
		return user;
		failed:
		return user;
	}
	else
	{
		std::cout << "Name oder Tag eingegeben." << std::endl;
		std::list<User> list = uDB.getUsersForSearchstring(
			password,
			eingabe);
		
		if(list.size() > 0)
		{
			std::string eing2;
			int selected = -1;
			do
			{
				int i = 0;
				std::cout << "Index\tVorname\tNachname\tBarcode"
					<< std::endl;
				for(auto & u : list)
				{
					std::cout << i
						<< "\t" << u.firstname
						<< "\t" << u.lastname
						<< "\t"	<< u.barcode << std::endl;
					i++;
				}
				std::cout << "Bitte Nutzer auswählen!" << std::endl;
				std::getline(std::cin, eing2);
				selected = std::stoi(eing2);
				int selected_iter = selected;
				for(auto iter = list.begin(); iter != list.end(); iter++)
				{
					if(selected_iter == 0)
					{
						user = *iter;
						print_user(user);
						return user;
						break;
					}
					selected_iter--;
				}
			} while (selected >= list.size() || selected < 0);
			return user; // if search failed
		}
		else
		{
			std::cout
				<< "Keine Nutzer mit diesem Namen oder Tag gefunden."
				<< std::endl;
			return user;
		}
	}
}

Book choose_book()
{
	Book book;
	std::string eingabe;
	std::cout << "Bitte ISBN einlesen." << std::endl;
	std::getline(std::cin, eingabe);
	if(is_numeric(eingabe))
	{
		book = bookcache.getBookByISBN(eingabe);
		if(book.authors == "" && book.title == "")
		{
			std::string eing2;
			do
			{
			std::cout << "Buchdaten zu ISBN herunterladen? (j/n)" << std::endl;
			std::getline(std::cin, eing2);
			} while(eing2 != "j" && eing2 != "n");
			if(eing2 == "j")
				book = lookupBook::lookupBook(eingabe, "bm-cli");
			if(book.ISBN == "")
				book.ISBN = eingabe;
			bookcache.updateBook(book);
		}
	}
	return book;
}

void lend_book()
{
	std::cout << "\nBuch ausleihen\n" << std::endl;
	User user = choose_user();
	if(user.barcode != "")
	{
		std::cout << "\nBuch ausleihen\n" << std::endl;
		Book book = choose_book();
		std::cout << book.title << " " << book.authors << std::endl;
		uDB.borrowBook(user, book.ISBN);
		std::cout << "Gebucht." << std::endl;
	}
}

void return_book()
{
	std::cout << "\nBuch zurückgeben\n"
		"\nISBN-Eingabe leer lassen um User-Dialog zu starten!\n"
		<< std::endl;
	Book book = choose_book();
	
	if(book.ISBN != "")
	{
		std::list<Borrowing> borrowings = uDB.getBorrowingsForISBN(book.ISBN);
		int iterator = 0;
		for(auto & b : borrowings)
		{
			User u = uDB.getUserByBarcode(b.user_barcode, password);
			intmax_t delta = get_current_utc() - b.date_time_utc_posix;
			std::cout << iterator << "\t" << u.firstname << "\t" << u.lastname << "\t"
				<< delta / (60.0 * 60 * 24) << " Tage" << std::endl;
			iterator++;
		}
		
		std::string eingabe;
		std::cout
			<< "Bitte die Nummer der Ausleihe eingeben."
			<< std::endl;
		std::getline(std::cin, eingabe);
		int selected = std::stoi(eingabe);
		iterator = 0;
		for(auto iter = borrowings.cbegin(); iter != borrowings.cend(); iter++)
		{
			if(selected == iterator)
			{
				uDB.returnBook(*iter);
				break;
			}
			iterator++;
		}
	}
	else // TODO: User-Dialog
	{
	}
}

void initialize_uDB()
{
	password = "1234";
	std::string userDB_filename = "users.sqlite";
	if(std::filesystem::exists(userDB_filename))
	{
		uDB.open(userDB_filename);
	}
	else
	{
		std::fstream file(userDB_filename, std::ios_base::out);
		file.close();
		uDB.open(userDB_filename);
		uDB.initializeDatabase(password);
	}
	if(!uDB.isValidDB())
	{
		std::cout << userDB_filename << " is invalid!" << std::endl;
		exit(1);
	}
	if(!uDB.passwordValid(password))
	{
		std::cout << userDB_filename << " password does not match!" << std::endl;
		exit(1);
	}
}

void initialize_BookDB()
{
	std::string BookDB_filename = "books.sqlite";
	if(std::filesystem::exists(BookDB_filename))
	{
		bookcache.open(BookDB_filename);
	}
	else
	{
		std::fstream file(BookDB_filename, std::ios_base::out);
		file.close();
		bookcache.open(BookDB_filename);
		bookcache.initializeDatabase();
	}
	if(!bookcache.isValid())
	{
		std::cout << BookDB_filename << " is invalid!" << std::endl;
		exit(1);
	}
}

void add_book_to_database()
{
	std::string isbn;
	std::getline(std::cin, isbn);
	if(!is_numeric(isbn) || isbn.length() != 13)
	{
		std::cout <<
			"Die Eingabe sieht nicht nach einer ISBN aus, breche ab.";
		return;
	}
	Book to_insert = lookupBook::lookupBook(isbn,
		"bm-cli+andreas.wagner+on+gitlab.com");
	
	if(to_insert.authors != "" && to_insert.title != "")
	{
		std::cout << "ISBN:\t" << to_insert.ISBN << std::endl
			<< "Titel:\t" << to_insert.title << std::endl
			<< "Author:\t" << to_insert.authors << std::endl;
		bookcache.updateBook(to_insert);
	}
	else
	{
		std::cout << "Fehler beim interpretieren der Daten.";
	}
}

int main(int argc, char *argv[])
{
	bool end = false;
	
	curl_global_init(CURL_GLOBAL_SSL);
	initialize_uDB();
	initialize_BookDB();

	while(!end)
	{
		std::string option;
		std::cout << "\n\n" << std::endl;
		std::cout << "1 Buch ausleihen" << std::endl;
		std::cout << "2 Buch zurückgeben" << std::endl;
		std::cout <<
		"5 Bücher-Datenbank ergänzen (Bücher mit ISBN abfragen)"
			<< std::endl;
		std::cout << "7 Bücher mit Verzug anzeigen" << std::endl;
		std::cout << "e Ende" << std::endl;
		std::getline(std::cin, option);
		std::cout << "\n\n" << std::endl;
		switch(option[0])
		{
			case '1':
				lend_book();
				break;
			case '2':
				return_book();
				break;
			case '5':
				add_book_to_database();
				break;
			case '7':
				break;
			case 'e':
				end = true;
				break;
			default:
				std::cout << "Hä? '" << option << "'?" << std::endl;
				break;
		}
	}
	uDB.close();
}


