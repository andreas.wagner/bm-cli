#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <bookcache.h>
#include <memory>

#include "userdb.h"
#include "prepareitemslist.h"

class MainWindow
{

private :
    void on_CommandLineEdit_returnPressed();

    void on_FilterLineEdit_returnPressed();

    void on_FilterBooksLineEdit_returnPressed();

    void on_CreateBookDBPushButton_clicked();

    void on_CreateBorrowerDBPushButton_clicked();

    void on_HidePasswordCheckBox_stateChanged(int arg1);

    void on_EditInventoryPushButton_clicked();

    void on_selectUsersToPrintBarcodesPushButton_clicked();

    void on_FilterBooksLineEdit_textChanged(const QString &arg1);

    void FilterBooksTimeout();

    void on_BooksLineEdit_editingFinished();

//    void on_importAndTagWithFilenamePushButton_clicked();

    void on_BorrowersLineEdit_editingFinished();

    void on_SelectBorrowerDatabaseFilePushButton_clicked();

    void on_passwordLineEdit_editingFinished();

    void on_SelectBookDatabaseFilePushButton_clicked();

    void on_FilterLineEdit_editingFinished();

    void on_UsersTableWidget_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);

    void on_applyPushButton_clicked();

    void on_cancelPushButton_clicked();

    void on_commandResultListWidget_customContextMenuRequested(const QPoint &pos);

    void on_commandResultListWidget_currentRowChanged(int currentRow);

    void flush_itemsFromCommand_to_ListItemWidget(QListWidget *widget);

    void searchBook(const std::string &str);

    void fill_BorrowedBooksListView(const std::string &str);

    std::string getBorrowingDescription(const Borrowing &b);

    void on_refreshListPushButton_clicked();

    void on_BorrowedBooksListWidget_customContextMenuRequested(const QPoint &pos);

    void on_borrowingDurationSpinBox_valueChanged(int arg1);

    void on_extraPeriodDurationSpinBox_valueChanged(int arg1);

    void on_accessTokenLineEdit_textChanged(const QString &arg1);

    void on_BarcodeLineEdit_textChanged(const QString &arg1);

    void on_addUserPushButton_clicked();

    void on_UsersTableWidget_customContextMenuRequested(const QPoint &pos);

    void on_ImportPushButton_clicked();

private:

    void searchUser(QString &str);
    void searchUserBarcode(QString &str);
    void handle_user_isbn_interaction(const std::string &str, const User &user);
    std::shared_ptr<BookCache> book_cache;
    std::shared_ptr<userDB> userdb;
    std::string config_file;
    std::list<QTableWidgetItem*> tableItemList;
    prepareItemsList *itemsFromCommand;
    std::vector<Borrowing> narrowdDownBorrowings;
    std::list<QListWidgetItem*> listItems_Borrowings;
    std::shared_ptr<std::map<std::string, std::string>> config;
};

#endif // MAINWINDOW_H
