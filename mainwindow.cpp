#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "createBookDB.h"
#include "createborrowerdbdialog.h"
#include "managebooksdialog.h"
#include "printuserbarcodesdialog.h"
#include "importusers_impl.h"

#include <iostream>
#include <fstream>

#include "recodestring.h"
#include "utc_time.h"
#include "prepareitemslist.h"
#include "getactiondescription.h"


void MainWindow::handle_user_isbn_interaction(const std::string &str, const User &user)
{
    std::list<Borrowing> borrowings(std::move(userdb->getBorrowingsForUser(user)));
    bool userAlreadyBorrowedThis = false;
    for(Borrowing borrowing : borrowings)
    {
        if(borrowing.isbn == str)
        {
            userAlreadyBorrowedThis = true;
            break;
        }
    }
    if(!userAlreadyBorrowedThis)
    {
        Book book(book_cache->getBookByISBN(str));
        std::string text(getActionDescription::borrowBook(user, book));
        ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
        userdb->borrowBook(user, str);

        if(book.title == "" && book.authors == "")
        {
            ManageBooksDialog mbd(book_cache, config, this, book.ISBN);
            mbd.exec();
        }
    }
    else
    {
        QMessageBox msg(QMessageBox::NoIcon, tr("Bereits geliehen!"), tr("Der Kunde hat das Buch bereits geliehen."), QMessageBox::NoButton, this);
        QPushButton lendAnotherButton(tr("Weiteres Exemplar leihen"));
        QPushButton returnBookButton(tr("Buch zurückgeben"));
        QPushButton cancelButton(tr("Abbrechen"));
        msg.addButton(&lendAnotherButton, QMessageBox::AcceptRole);
        msg.addButton(&cancelButton, QMessageBox::AcceptRole);
        msg.addButton(&returnBookButton, QMessageBox::AcceptRole);
        msg.exec();
        if(msg.clickedButton() == &lendAnotherButton)
        {
            Book book(book_cache->getBookByISBN(str));
            std::string text(getActionDescription::borrowBook(user, book));
            ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
            userdb->borrowBook(user, str);
        }
        else if(msg.clickedButton() == &returnBookButton)
        {
            std::list<Borrowing> bor1(std::move(userdb->getBorrowingsForUser(user)));
            std::list<Borrowing> list_of_borrowings_with_same_isbn;
            for(Borrowing b : bor1)
            {
                if(b.isbn == str)
                {
                    list_of_borrowings_with_same_isbn.push_back(b);
                }
            }
            time_t minimum = list_of_borrowings_with_same_isbn.front().date_time_utc_posix;
            Borrowing to_return = list_of_borrowings_with_same_isbn.front();
            for(Borrowing b : list_of_borrowings_with_same_isbn)
            {
                if(b.date_time_utc_posix < minimum)
                {
                    minimum = b.date_time_utc_posix;
                    to_return = b;
                }
            }
            std::string text(getActionDescription::returnBook(to_return,
                                                              *book_cache,
                                                              *userdb,
                                                              ui->passwordLineEdit->text().toUtf8().constData(),
                                                              ui->borrowingDurationSpinBox->value(),
                                                              ui->extraPeriodDurationSpinBox->value()));
            userdb->returnBook(to_return);
            ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
        }
    }
}




void MainWindow::flush_itemsFromCommand_to_ListItemWidget(QListWidget *widget)
{
    for(int i = 0; i < itemsFromCommand->getNumberOfItems(); i++)
    {
        if(itemsFromCommand->getItem(i).type == item::BORROWING)
        {
            Borrowing *bor = itemsFromCommand->getItem(i).borrowing;
            Book book(book_cache->getBookByISBN(bor->isbn));
            QListWidgetItem *newItem = new QListWidgetItem(
                        std::string(u8"   →»" + book.title + u8"«, " + book.authors + ": " + bor->isbn + "\n  seit " + get_localtime_from_utc(bor->date_time_utc_posix) + " Verl.: " + std::to_string(bor->extra_periods)
                                    ).c_str());
            widget->addItem(newItem); // FIXME: will that be deleted by Qt?
        }
        else if(itemsFromCommand->getItem(i).type == item::USER)
        {
            User *usr = itemsFromCommand->getItem(i).user;
            QListWidgetItem *newItem = new QListWidgetItem(
                        std::string(usr->firstname + " " + usr->lastname
                                    ).c_str());
            widget->addItem(newItem);
        }
        else {
            throw new std::logic_error("item.type neither BORRWING nor USER.");
        }
    }
}


void MainWindow::searchUserBarcode(QString &str)
{
    if(itemsFromCommand != nullptr)
    {
        ui->commandResultListWidget->clear();
        delete itemsFromCommand;
    }
    itemsFromCommand = new prepareItemsList(prepareItemsList::SEARCH_BARCODE,
                                            str.toUtf8().constData(),
                                            userdb,
                                            ui->passwordLineEdit->text().toUtf8().constData());
    flush_itemsFromCommand_to_ListItemWidget(ui->commandResultListWidget);
}

void MainWindow::searchUser(QString &str)
{
    if(itemsFromCommand != nullptr)
    {
        ui->commandResultListWidget->clear();
        delete itemsFromCommand;
    }
    itemsFromCommand = new prepareItemsList(prepareItemsList::SEARCH_QUERY,
                                            str.toUtf8().constData(),
                                            userdb,
                                            ui->passwordLineEdit->text().toUtf8().constData());
    flush_itemsFromCommand_to_ListItemWidget(ui->commandResultListWidget);
}




void MainWindow::searchBook(const std::string &str)
{
    if(itemsFromCommand != nullptr)
    {
        ui->commandResultListWidget->clear();
        delete itemsFromCommand;
    }
    itemsFromCommand = new prepareItemsList(prepareItemsList::SEARCH_BORROWINGS,
                                            str,
                                            userdb,
                                            ui->passwordLineEdit->text().toUtf8().constData());
    flush_itemsFromCommand_to_ListItemWidget(ui->commandResultListWidget);
}



MainWindow::MainWindow(QWidget *parent) :
    itemsFromCommand(nullptr)
{
    ui->UsersTableWidget->setColumnCount(3);

    QStringList horizontalHeaders{
        "Vorname",
        "Nachname",
        "Barcode"
    };
    ui->UsersTableWidget->setHorizontalHeaderLabels(horizontalHeaders);
    config = std::make_shared<std::map<std::string, std::string>>();

    recodeString::initialize();

    connect(&waitForInputOnFilterBooks, SIGNAL(timeout()), this, SLOT(FilterBooksTimeout()));

    book_cache = std::make_shared<BookCache>();
    userdb = std::make_shared<userDB>();

    // read config from config-file
    std::list<std::string> standard_config_files;
    standard_config_files.push_back("Borrowing_Manager_v2.config");
    standard_config_files.push_back("~/Borrowing_Manager_v2.config");
    standard_config_files.push_back("/etc/Borrowing_Manager_v2.config");
    std::list<std::string>::iterator file_iterator = standard_config_files.begin();
    do
    {
        std::string filename(*file_iterator);
        if(QFile::exists(filename.c_str()))
        {
            file_iterator = standard_config_files.end();
            config_file = filename;
            std::ifstream input(filename);
            std::string line;
            while(std::getline(input, line))
            {
                size_t whitespace_pos = line.find(' ');
                if(whitespace_pos != std::string::npos)
                {
                    std::string command = line.substr(0, whitespace_pos);
                    std::string parameter = line.substr(whitespace_pos+1);
                    (*config)[command] = parameter;
                }
            }
        }
        if(file_iterator != standard_config_files.end()) file_iterator++;
    }
    while (file_iterator != standard_config_files.end());

    if(config->count("BorrowerDB") != 0)
    {
        ui->BorrowersLineEdit->setText((*config)["BorrowerDB"].c_str());
        on_BorrowersLineEdit_editingFinished();
    }
    if(config->count("BookDB") != 0)
    {
        ui->BooksLineEdit->setText((*config)["BookDB"].c_str());
        on_BooksLineEdit_editingFinished();
    }

    if(config->count("BorrowingDuration") != 0)
    {
        int duration = std::atoi((*config)["BorrowingDuration"].c_str());
        ui->borrowingDurationSpinBox->setValue(duration);
    }
    if(config->count("ExtraPeriodDuration") != 0)
    {
        int duration = std::atoi((*config)["ExtraPeriodDuration"].c_str());
        ui->extraPeriodDurationSpinBox->setValue(duration);
    }

    if(config->count("AccessToken") != 0)
    {
        ui->accessTokenLineEdit->setText((*config)["AccessToken"].c_str());
    }

    ui->encodingComboBox->addItem("Latin1");
    ui->encodingComboBox->addItem("UTF-8");

    ui->passwordLineEdit->setFocus();
}

MainWindow::~MainWindow()
{
    // write config to file
    QFileInfo fileInfo(config_file.c_str());
    if(fileInfo.isWritable())
    {
        std::ofstream output(config_file);
        for(std::pair<std::string, std::string> entry : *config)
        {
            output << entry.first << " " << entry.second << std::endl;
        }
    }

    recodeString::finalize();

    delete ui;
}

bool isNumeric(const std::string &str)
{
    bool retVal = true;
    for(int i = 0; i < str.size(); i++)
    {
        if(!isdigit(str[i]))
        {
            retVal = false;
            break;
        }
    }
    return retVal;
}

void MainWindow::on_CommandLineEdit_returnPressed()
{
    QString str = ui->CommandLineEdit->text();
    ui->CommandLineEdit->setText("");
    ui->AdditionalInfoPlainTextEdit->setPlainText("");
    int length = str.length();
    if((length == 10 || length == 13) && isNumeric(str.toUtf8().constData()))
    {
        // Auswerten, ob ein Benutzer ausgewählt ist; ggf. diesem Benutzer ein
        // Buch zuordnen.
        int item_index = ui->commandResultListWidget->currentRow();
        if(item_index > -1)
        {
            item myItem = itemsFromCommand->getItem(item_index);
            if(myItem.type == item::USER)
            {
                User user(*myItem.user);
                handle_user_isbn_interaction(str.toUtf8().constData(), user);
                ui->commandResultListWidget->clear();
            }
            else if(myItem.type == item::BORROWING)
            {
                Borrowing borrowing(*myItem.borrowing);
                User user(userdb->getUserByBarcode(borrowing.user_barcode,
                                                   ui->passwordLineEdit->text().toUtf8().constData()));
                handle_user_isbn_interaction(str.toUtf8().constData(), user);
                ui->commandResultListWidget->clear();
            }
            else
            {
                throw new std::logic_error("Item was neither of type BORROWING nor USER.");
            }
        }
        else // nothing selected
        {
            searchBook(str.toUtf8().constData());
            ui->commandResultListWidget->setFocus();
            ui->commandResultListWidget->setCurrentRow(0);
        }
    }
    else if(length == 8 && isNumeric(str.toUtf8().constData()))
    {
        searchUserBarcode(str);
        ui->commandResultListWidget->setFocus();
        ui->commandResultListWidget->setCurrentRow(0);
        ui->CommandLineEdit->setFocus();
    }
    else
    {
        searchUser(str);
        if(ui->commandResultListWidget->count() > 0)
        {
            ui->commandResultListWidget->setFocus();
            ui->commandResultListWidget->setCurrentRow(0);
        }
    }
}



void MainWindow::on_FilterLineEdit_returnPressed()
{
}

void MainWindow::on_FilterBooksLineEdit_returnPressed()
{
    waitForInputOnFilterBooks.stop();
    QString str = ui->FilterBooksLineEdit->text();
    fill_BorrowedBooksListView(str.toUtf8().constData());
}

void MainWindow::on_CreateBookDBPushButton_clicked()
{
    CreateBookDBDialog dialog(this);
    dialog.exec();
}

void MainWindow::on_CreateBorrowerDBPushButton_clicked()
{
    CreateBorrowerDBDialog dialog(this);
    dialog.exec();
}

void MainWindow::on_HidePasswordCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Unchecked)
    {
        MainWindow::ui->passwordLineEdit->setEchoMode(QLineEdit::EchoMode::Normal);
    }
    else
    {
        MainWindow::ui->passwordLineEdit->setEchoMode(QLineEdit::EchoMode::Password);
    }
}

void MainWindow::on_EditInventoryPushButton_clicked()
{
    ManageBooksDialog dialog(book_cache, config, this);
    dialog.exec();
}

void MainWindow::on_selectUsersToPrintBarcodesPushButton_clicked()
{
    PrintUserBarcodesDialog dialog(this, config, userdb, ui->passwordLineEdit->text().toUtf8().constData());
    dialog.exec();
}

void MainWindow::on_FilterBooksLineEdit_textChanged(const QString &arg1)
{
    (void)arg1; //suppress warning of unused variable
    waitForInputOnFilterBooks.start(333);
}

void MainWindow::FilterBooksTimeout()
{
    waitForInputOnFilterBooks.stop();
    QString str = ui->FilterBooksLineEdit->text();
    fill_BorrowedBooksListView(str.toUtf8().constData());
}

void MainWindow::on_BooksLineEdit_editingFinished()
{
    QString filename(ui->BooksLineEdit->text());
    (*config)["BookDB"] = filename.toUtf8().constData();
    try
    {
        book_cache->open(filename.toUtf8().constData());
        if(book_cache->isValid())
        {
            ui->bookDBValidLabel->setText(u8"☑");
        }
        else
        {
            ui->bookDBValidLabel->setText(u8"☐");
        }
    }
    catch(std::runtime_error &e)
    {
        (void)e;
        ui->bookDBValidLabel->setText(u8"☐");
    }
}

/*
void MainWindow::on_importAndTagWithFilenamePushButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Alle Dateien (*);;CSV-Datei (*.csv)"));
    std::string filename;
    if(dialog.exec())
    {
        std::string password = ui->passwordLineEdit->text().toUtf8().constData();
        QStringList list = dialog.selectedFiles();
        for(QString item : list)
        {
            std::string filename = item.toUtf8().constData();
            std::ifstream input;
            std::string line;
            std::string tag;
            std::list<std::string> tag_list;
            size_t last_dot_pos = filename.find_last_of('.');
            size_t last_slash_pos = filename.find_last_of('/');

            // FIXME: enable ~/data.sqlite/borrowers
            if(last_slash_pos != std::string::npos)
            {
                if(last_dot_pos != std::string::npos)
                {
                    tag = filename.substr(last_slash_pos+1, last_dot_pos-last_slash_pos-1);
                }
                else
                {
                    tag = filename.substr(last_slash_pos+1);
                }
            }
            else
            {
                if(last_dot_pos != std::string::npos)
                {
                    tag = filename.substr(0, last_dot_pos);
                }
                else
                {
                    tag = filename;
                }
            }
            tag_list.push_back(tag);
            input.open(filename);
            while(std::getline(input, line))
            {
                User u;
                line = recodeString::toUtf8(ui->encodingComboBox->currentText().toUtf8().constData(), line);
                size_t delimiter_pos = line.find(';');
                u.firstname = line.substr(0, delimiter_pos);
                u.lastname = line.substr(delimiter_pos + 1);
                if(u.lastname.find('\r') != std::string::npos)
                {
                    u.lastname = u.lastname.substr(0, u.lastname.find('\r'));
                }
                std::string id = userdb->addUser(u, password);
                u = userdb->getUserByBarcode(id, password);
                userdb->setTagsForUser(tag_list, u);
            }
            input.close();
        }
    }
}
*/

void MainWindow::on_BorrowersLineEdit_editingFinished()
{
    std::string filename(ui->BorrowersLineEdit->text().toUtf8().constData());
    QFile file(filename.c_str());
    (*config)["BorrowerDB"] = filename.c_str();
    if(file.exists())
    {
        userdb->open(filename);
        if(userdb->isValidDB())
        {
            ui->borrowersDBValidLabel->setText(u8"☑");
        }
        else
        {
            ui->borrowersDBValidLabel->setText(u8"☐");
        }
    }
    else
    {
        ui->borrowersDBValidLabel->setText(u8"☐");
    }
}

void MainWindow::on_SelectBorrowerDatabaseFilePushButton_clicked()
{
    std::string filename(QFileDialog::getOpenFileName(
                             this,
                             tr("Entleiher-Datenbank auswählen"),
                             "",
                             tr("SQLite-Datei (*.sqlite);; Alle Dateien (*)")
                             ).toUtf8().constData());
    ui->BorrowersLineEdit->setText(filename.c_str());
    this->on_BorrowersLineEdit_editingFinished();
}

void MainWindow::on_passwordLineEdit_editingFinished()
{
    std::string password(ui->passwordLineEdit->text().toUtf8().constData());
    if(userdb->passwordValid(password))
    {
        ui->passwordValidLabel->setText(u8"☑");
        ui->lendingTab->setEnabled(true);
        ui->usersTab->setEnabled(true);
        ui->borrowedBooksTab->setEnabled(true);
        ui->groupBox->setEnabled(true);
        ui->selectUsersToPrintBarcodesPushButton->setEnabled(true);
    }
    else
    {
        ui->passwordValidLabel->setText(u8"☐");
        ui->lendingTab->setEnabled(false);
        ui->usersTab->setEnabled(false);
        ui->borrowedBooksTab->setEnabled(false);
        ui->groupBox->setEnabled(false);
        ui->selectUsersToPrintBarcodesPushButton->setEnabled(false);
    }
    ui->HidePasswordCheckBox->setCheckState(Qt::Checked);
}

void MainWindow::on_SelectBookDatabaseFilePushButton_clicked()
{
    std::string filename(QFileDialog::getOpenFileName(
                             this,
                             tr(u8"Bücher-Datenbank auswählen"),
                             "",
                             tr("SQLite-Datei (*.sqlite);; Alle Dateien (*)")
                             ).toUtf8().constData());
    ui->BooksLineEdit->setText(filename.c_str());
    this->on_BooksLineEdit_editingFinished();
}

void MainWindow::on_FilterLineEdit_editingFinished()
{
    for(QTableWidgetItem *item : tableItemList)
    {
        delete item;
    }
    tableItemList.clear();
    std::list<User> userList = userdb->getUsersForSearchstring(
                ui->passwordLineEdit->text().toUtf8().constData(),
                ui->FilterLineEdit->text().toUtf8().constData()
                );
    ui->UsersTableWidget->clear();
    ui->UsersTableWidget->setColumnCount(3);
    QStringList horizontalHeaders{
        "Vorname",
        "Nachname",
        "Barcode"
    };
    ui->UsersTableWidget->setHorizontalHeaderLabels(horizontalHeaders);
    ui->UsersTableWidget->setRowCount(userList.size());
    int rowCount = 0;
    for(User u : userList)
    {
        QTableWidgetItem *item1 = new QTableWidgetItem(u.firstname.c_str());
        item1->setFlags(item1->flags() & ~Qt::ItemIsEditable);
        QTableWidgetItem *item2 = new QTableWidgetItem(u.lastname.c_str());
        item2->setFlags(item2->flags() & ~Qt::ItemIsEditable);
        QTableWidgetItem *item3 = new QTableWidgetItem(u.barcode.c_str());
        item3->setFlags(item3->flags() & ~Qt::ItemIsEditable);

        ui->UsersTableWidget->setItem(rowCount, 0, item1);
        ui->UsersTableWidget->setItem(rowCount, 1, item2);
        ui->UsersTableWidget->setItem(rowCount, 2, item3);

        tableItemList.push_back(item1);
        tableItemList.push_back(item2);
        tableItemList.push_back(item3);
        rowCount++;
    }
}

void MainWindow::on_UsersTableWidget_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *)
{
    int row = current->row();
    std::string barcode = ui->UsersTableWidget->item(row, 2)->text().toUtf8().constData();
    std::string password = ui->passwordLineEdit->text().toUtf8().constData();
    User u = userdb->getUserByBarcode(barcode,password);
    std::list<std::string> tags = userdb->getTagsForUser(u);
    ui->BarcodeLineEdit->setText(u.barcode.c_str());
    ui->FirstNameLineEdit->setText(u.firstname.c_str());
    ui->LastNameLineEdit->setText(u.lastname.c_str());
    ui->TagsPlainTextEdit->clear();
    std::string tagString;
    for(std::string str : tags)
    {
        tagString += str + "\n";
    }
    ui->TagsPlainTextEdit->setPlainText(tagString.c_str());
}

void MainWindow::on_applyPushButton_clicked()
{
    User u;
    std::list<std::string> tag_list;
    std::string tagString = ui->TagsPlainTextEdit->toPlainText().toUtf8().constData();
    size_t iterator = 0;
    size_t old_iterator = 0;
    while(iterator != std::string::npos)
    {
        old_iterator = iterator;
        iterator = tagString.find('\n', iterator);
        std::string toAdd = tagString.substr(old_iterator, iterator-old_iterator);
        if(!toAdd.empty())
            tag_list.push_back(toAdd);
        if(iterator != std::string::npos)
            iterator++;
    }
    std::string password = ui->passwordLineEdit->text().toUtf8().constData();
    u.barcode = ui->BarcodeLineEdit->text().toUtf8().constData();
    u.firstname = ui->FirstNameLineEdit->text().toUtf8().constData();
    u.lastname = ui->LastNameLineEdit->text().toUtf8().constData();
    userdb->updateUser(u, password);
    userdb->setTagsForUser(tag_list, u);
    on_FilterLineEdit_editingFinished();
}

void MainWindow::on_cancelPushButton_clicked()
{
    ui->BarcodeLineEdit->clear();
    ui->FirstNameLineEdit->clear();
    ui->LastNameLineEdit->clear();
    ui->TagsPlainTextEdit->clear();
}




void MainWindow::on_commandResultListWidget_customContextMenuRequested(const QPoint &pos)
{
    int index = ui->commandResultListWidget->indexAt(pos).row();
    item myItem = itemsFromCommand->getItem(index);
    if(index > -1 && myItem.type == item::BORROWING)
    {
        QMenu menu(ui->commandResultListWidget);
        QAction *returnBook = menu.addAction(tr("Buch zurückgeben"));
        QAction *extraPeriod = menu.addAction(tr("Ausleihe verlängern"));
        QAction *retVal = menu.exec(ui->commandResultListWidget->mapToGlobal(pos));
        if(retVal == returnBook)
        {
            std::string text(getActionDescription::returnBook(*myItem.borrowing,
                                                              *book_cache,
                                                              *userdb,
                                                              ui->passwordLineEdit->text().toUtf8().constData(),
                                                              ui->borrowingDurationSpinBox->value(),
                                                              ui->extraPeriodDurationSpinBox->value()));
            ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
            userdb->returnBook(*myItem.borrowing);
            ui->commandResultListWidget->clear();
        }
        else if(retVal == extraPeriod)
        {

            userdb->incrementExtraPeriods(*myItem.borrowing);
            std::string text(getActionDescription::incrementExtraPeriods(true,
                                                                         *myItem.borrowing,
                                                                         *book_cache,
                                                                         *userdb,
                                                                         ui->passwordLineEdit->text().toUtf8().constData(),
                                                                         ui->borrowingDurationSpinBox->value(),
                                                                         ui->extraPeriodDurationSpinBox->value()));
            ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
            ui->commandResultListWidget->clear();
        }
        else // none of the options used
        {
        }
    }
}

void MainWindow::on_commandResultListWidget_currentRowChanged(int currentRow)
{
    if(currentRow > -1)
    {
        item myItem = itemsFromCommand->getItem(currentRow);
        std::string text;
        if(myItem.type == item::BORROWING)
        {
            User user = userdb->getUserByBarcode(myItem.borrowing->user_barcode,
                                                 ui->passwordLineEdit->text().toUtf8().constData());
            Book book = book_cache->getBookByISBN(myItem.borrowing->isbn);
            text = user.firstname + " " + user.lastname + "\n"
                    + u8"»"+ book.title + u8"« von " + book.authors + " ISBN:"
                    + myItem.borrowing->isbn
                    + "\nseit "
                    + get_localtime_from_utc(myItem.borrowing->date_time_utc_posix)
                    + " bei " + std::to_string(myItem.borrowing->extra_periods) + " Verlängerungen.\n";
            text += "Fällig am: " + get_localtime_from_utc(
                        myItem.borrowing->date_time_utc_posix
                        + ui->borrowingDurationSpinBox->value() * 60 * 60
                        + ui->extraPeriodDurationSpinBox->value() * 60 * 60 * myItem.borrowing->extra_periods);

        }
        else if(myItem.type == item::USER)
        {
            std::list<Borrowing> borrowings(std::move(userdb->getBorrowingsForUser(*myItem.user)));
            std::list<std::string> usersTags(std::move(userdb->getTagsForUser(*myItem.user)));
            text = myItem.user->firstname + " " + myItem.user->lastname + "\n";
            int numberOfTags = usersTags.size();
            for(std::string tag : usersTags)
            {
                text += tag;
                if(numberOfTags > 1)
                {
                    text += "; ";
                    numberOfTags--;
                }
            }
            text += "\nhat geliehen:\n";
            for(Borrowing b : borrowings)
            {
                Book book(book_cache->getBookByISBN(b.isbn));
                text += u8"  • »" + book.title + u8"« von "
                        + book.authors + " seit "
                        + get_localtime_from_utc(b.date_time_utc_posix)
                        + " mit " + std::to_string(b.extra_periods)
                        +" Verlängerungen\n";
            }
        }
        else
        {

        }
        ui->AdditionalInfoPlainTextEdit->setPlainText(text.c_str());
    }
}

std::string MainWindow::getBorrowingDescription(const Borrowing &b)
{
    std::string retVal;
    retVal.reserve(512);
    User user(userdb->getUserByBarcode(b.user_barcode,
                                       ui->passwordLineEdit->text().toUtf8().constData()));
    Book book(book_cache->getBookByISBN(b.isbn));
    time_t due = b.date_time_utc_posix
            + ui->borrowingDurationSpinBox->value() * 60 * 60
            + b.extra_periods * ui->extraPeriodDurationSpinBox->value() *60*60;
    if(due < get_current_utc())
    {
        retVal = " →";
    }
    retVal += "Fällig am: " + get_localtime_from_utc(due) + "\n";
    retVal += user.firstname + " " + user.lastname + ": "
            + book.title + ", " + book.authors + " "
            + "Verl. " + std::to_string(b.extra_periods) + " "
            + book.ISBN + " " + user.barcode;
    return retVal;
}

void MainWindow::fill_BorrowedBooksListView(const std::string &str)
{
    for(QListWidgetItem* ptr : listItems_Borrowings)
    {
        delete ptr;
    }
    listItems_Borrowings.clear();
    ui->BorrowedBooksListWidget->clear();
    std::list<Borrowing> borrowings(userdb->getAllBorrowings());
    narrowdDownBorrowings.clear();
    narrowdDownBorrowings.reserve(borrowings.size());
    for(Borrowing b : borrowings)
    {
        QListWidgetItem * newItem = nullptr;
        if(str.empty())
        {
            narrowdDownBorrowings.push_back(b);
            newItem = new QListWidgetItem(getBorrowingDescription(b).c_str());
            ui->BorrowedBooksListWidget->addItem(newItem);
        }
        else
        {
            std::string borrowingDesc(getBorrowingDescription(b));
            if(borrowingDesc.find(str) != std::string::npos)
            {
                narrowdDownBorrowings.push_back(b);
                newItem = new QListWidgetItem(borrowingDesc.c_str());
                ui->BorrowedBooksListWidget->addItem(newItem);
            }
        }
        if(newItem != nullptr)
        {
            listItems_Borrowings.push_back(newItem);
        }
    }
}

void MainWindow::on_refreshListPushButton_clicked()
{
    fill_BorrowedBooksListView(
                ui->FilterLineEdit->text().toUtf8().constData());
}

void MainWindow::on_BorrowedBooksListWidget_customContextMenuRequested(const QPoint &pos)
{
    int index = ui->BorrowedBooksListWidget->indexAt(pos).row();

    if(index >-1)
    {
        QMenu menu(ui->BorrowedBooksListWidget);
        QAction *returnBook = menu.addAction(tr("Buch zurückgeben"));
        QAction *extraPeriod = menu.addAction(tr("Ausleihe verlängern"));
        QAction *retVal = menu.exec(ui->BorrowedBooksListWidget->mapToGlobal(pos));
        if(retVal == returnBook)
        {
            userdb->returnBook(narrowdDownBorrowings[index]);
            ui->commandResultListWidget->clear();
            fill_BorrowedBooksListView(
                        ui->FilterLineEdit->text().toUtf8().constData());
        }
        else if(retVal == extraPeriod)
        {
            userdb->incrementExtraPeriods(narrowdDownBorrowings[index]);
            ui->commandResultListWidget->clear();
            fill_BorrowedBooksListView(
                        ui->FilterLineEdit->text().toUtf8().constData());
        }
        else // none of the options used
        {
        }
    }
}

void MainWindow::on_borrowingDurationSpinBox_valueChanged(int arg1)
{
    (*config)["BorrowingDuration"] = std::to_string(arg1);
}

void MainWindow::on_extraPeriodDurationSpinBox_valueChanged(int arg1)
{
    (*config)["ExtraPeriodDuration"] = std::to_string(arg1);
}

void MainWindow::on_accessTokenLineEdit_textChanged(const QString &arg1)
{
    (*config)["AccessToken"] = arg1.toUtf8().constData();
}

void MainWindow::on_BarcodeLineEdit_textChanged(const QString &arg1)
{
    if(arg1.count() == 0)
    {
        ui->addUserPushButton->setEnabled(true);
        ui->applyPushButton->setEnabled(false);
    }
    else
    {
        ui->addUserPushButton->setEnabled(false);
        ui->applyPushButton->setEnabled(true);
    }
}

void MainWindow::on_addUserPushButton_clicked()
{
    std::string password = ui->passwordLineEdit->text().toUtf8().constData();
    User u;
    std::list<std::string> taglist;
    u.firstname = ui->FirstNameLineEdit->text().toUtf8().constData();
    u.lastname = ui->LastNameLineEdit->text().toUtf8().constData();
    u.barcode = userdb->addUser(u, password);

    std::string tagString = ui->TagsPlainTextEdit->toPlainText().toUtf8().constData();
    size_t iterator = 0;
    size_t old_iterator = 0;
    while(iterator != std::string::npos)
    {
        old_iterator = iterator;
        iterator = tagString.find('\n', iterator);
        std::string toAdd = tagString.substr(old_iterator, iterator-old_iterator);
        if(!toAdd.empty())
            taglist.push_back(toAdd);
        if(iterator != std::string::npos)
            iterator++;
    }

    userdb->setTagsForUser(taglist, u);

    ui->FirstNameLineEdit->clear();
    ui->LastNameLineEdit->clear();
    ui->BarcodeLineEdit->clear();
    ui->TagsPlainTextEdit->clear();
}

void MainWindow::on_UsersTableWidget_customContextMenuRequested(const QPoint &pos)
{
    int index = ui->UsersTableWidget->indexAt(pos).row();
    if(index > -1)
    {
        QTableWidgetItem *item = ui->UsersTableWidget->item(index, 2);
        User u;
        u.barcode = item->text().toUtf8().constData();
        QMenu menu(ui->UsersTableWidget);
        QAction *removeUser = menu.addAction(tr("Benutzer entfernen"));
        QAction *retVal = menu.exec(ui->commandResultListWidget->mapToGlobal(pos));
        if(retVal == removeUser)
        {
            userdb->removeUser(u);
            ui->FilterLineEdit->clear();
            ui->FirstNameLineEdit->clear();
            ui->LastNameLineEdit->clear();
            ui->BarcodeLineEdit->clear();
            ui->TagsPlainTextEdit->clear();
            on_FilterLineEdit_editingFinished();
        }
    }

}

void MainWindow::on_ImportPushButton_clicked()
{
    importUsers_Impl import(this, userdb, ui->passwordLineEdit->text().toUtf8().constData(), ui->encodingComboBox->currentText().toUtf8().constData());
    import.exec();
}
