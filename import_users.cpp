#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>

#include "userdb.h"

userDB uDB;
std::string password;

void print_user(User &user)
{
	std::list<std::string> tags;
	try {
		tags = uDB.getTagsForUser(user);
	}	
	catch(std::runtime_error e)
	{
		std::cout << e.what() << std::endl;
		goto failed;
	}
	std::cout << "\nTags:";
	for(auto & t : tags)
	{
		std::cout << " " << t ;
	}
	std::cout << std::endl;
	std::cout << "Vorname: " << user.firstname << std::endl;
	std::cout << "Nachname: " << user.lastname << std::endl;
	failed:
	;
}

int main(int argc, char * argv[])
{
	password = "1234";
	std::string userDB_filename = "users.sqlite";
	if(std::filesystem::exists(userDB_filename))
	{
		uDB.open(userDB_filename);
	}
	else
	{
		std::fstream file(userDB_filename, std::ios_base::out);
		file.close();
		uDB.open(userDB_filename);
		uDB.initializeDatabase(password);
	}
	if(!uDB.isValidDB())
	{
		std::cout << userDB_filename << " is invalid!" << std::endl;
		return 1;
	}
	if(!uDB.passwordValid(password))
	{
		std::cout << userDB_filename << " password does not match!" << std::endl;
		return 1;
	}

	for(int i = 1; i < argc; i++)
    {
		std::string filename(argv[i]);
        std::ifstream input;
        std::string line;
        std::string tag;
        std::list<std::string> tag_list;
        size_t last_dot_pos = filename.find_last_of('.');
        size_t last_slash_pos = filename.find_last_of('/');

		if(last_slash_pos != std::string::npos)
        {
            if(last_dot_pos != std::string::npos)
            {
                tag = filename.substr(last_slash_pos+1, last_dot_pos-last_slash_pos-1);
            }
            else
            {
                tag = filename.substr(last_slash_pos+1);
            }
        }
        else
        {
            if(last_dot_pos != std::string::npos)
            {
                tag = filename.substr(0, last_dot_pos);
            }
            else
            {
                tag = filename;
            }
        }
        tag_list.push_back(tag);

        input.open(filename);
        while(std::getline(input, line))
        {
            User u;
            //line = recodeString::toUtf8(encoding, line);
            size_t delimiter_pos = line.find(';');
            u.firstname = line.substr(0, delimiter_pos);
            u.lastname = line.substr(delimiter_pos + 1);
            if(u.lastname.find('\r') != std::string::npos)
            {
                u.lastname = u.lastname.substr(0, u.lastname.find('\r'));
            }
            std::string id = uDB.addUser(u, password);
            u = uDB.getUserByBarcode(id, password);
            uDB.setTagsForUser(tag_list, u);
			print_user(u);
        }
        input.close();
    }
	return 0;
}

