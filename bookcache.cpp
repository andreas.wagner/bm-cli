#include "bookcache.h"
//#include "urlfetch.h"
//#include "lookupbook.h"

#include <stdexcept>
//#include <chrono>
//#include <thread>
#include <list>
#include <set>
#include <filesystem>

BookCache::BookCache()
{
}

BookCache::~BookCache()
{
}

bool BookCache::isValid()
{
    std::set<std::string> tables;
    tables.insert("books");
    try{
        Statement s = connection.prepareStatement("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';");
        while(s.step())
        {
            std::string table_name = s.getText(0);
            if(tables.find(table_name) != tables.end())
            {
                tables.erase(table_name);
            }
        }
    }
    catch(std::exception *e)
    {
        return false;
    }
    if(tables.empty())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void BookCache::open(const std::string &filename)
{
    std::filesystem::path path(filename);
    if(std::filesystem::exists(path))
    {
        connection.open(filename);
    }
	else
	{
		std::cout << "Could not open " << filename << std::endl;
	}
}

void BookCache::close()
{
    connection.close();
}

void BookCache::initializeDatabase()
{
    Statement s = connection.prepareStatement("CREATE TABLE books(isbn TEXT PRIMARY KEY, authors TEXT, title TEXT);");
    s.step();
    s.finalize();
}

Book BookCache::getBookByISBN(const std::string &isbn) const
{
    Book book;
    book.ISBN = "";
    book.authors = "";
    book.title = "";
    Statement s = connection.prepareStatement("SELECT isbn, authors, title FROM books WHERE isbn = ?;");
    s.bind(1, isbn);
    if(s.step())
    {
        book.ISBN = s.getText(0);
        book.authors = s.getText(1);
        book.title = s.getText(2);
    }
    else
    {
        book.ISBN = isbn;
    }
    if(s.step())
    {
        throw new std::runtime_error("BookCache: s.step() brought another row; ISBN not unique?");
    }

    s.finalize();
    return book;
}

std::list<Book> BookCache::getAllBooks()
{
    std::list<Book> bookList;

    Statement s = connection.prepareStatement("SELECT isbn, authors, title FROM books;");

    while(s.step())
    {
        Book book;
        book.ISBN = s.getText(0);
        book.authors = s.getText(1);
        book.title = s.getText(2);

        bookList.push_back(book);
    }
    s.finalize();
    return bookList;
}

void BookCache::updateBook(const Book &book) const
{
    Statement s = connection.prepareStatement(
                "INSERT OR REPLACE INTO books(isbn, authors, title) VALUES(?, ?, ?)");
    s.bind(1, book.ISBN);
    s.bind(2, book.authors);
    s.bind(3, book.title);
    s.step();
    s.finalize();
}
