#ifndef USERDB_H
#define USERDB_H

#include "dbconnection.h"
#include <string>
#include <list>



std::string decode(const std::string &password, const Blob &blob);

Blob encode(const std::string &password, const std::string &text);

struct Borrowing
{
    std::string user_barcode;
    std::string isbn;
    time_t date_time_utc_posix;
    int64_t extra_periods;
};

struct User
{
    std::string barcode;
    std::string firstname;
    std::string lastname;
};

class userDB
{
public:
    userDB();
    void open(const std::string &filename);
    void close();

    void initializeDatabase(const std::string &password);

    bool isValidDB();
    bool passwordValid(const std::string &password);

    std::list<std::string> getTagsForUser(const User &user) const;
    void setTagsForUser(const std::list<std::string> &tags, const User &user);
    User getUserByBarcode(const std::string &barcode, const std::string &password) const;
    void updateUser(const User &user, const std::string &password);
    std::string addUser(const User &user, const std::string &password);
    void removeUser(const User &user); // TODO: überlegen, ob da auch ein passwort erfraget werden
                                    // sollte; wenn man nur nach dem Barcode sucht, braucht man das
                                    // nicht.

    std::list<User> getUsersForSearchstring(const std::string & password, const std::string &str) const;

    std::list<Borrowing> getBorrowingsForUser(const User &user) const;
    std::list<Borrowing> getBorrowingsForISBN(const std::string &isbn);
    void incrementExtraPeriods(Borrowing b);
    std::list<Borrowing> getAllBorrowings();

    void borrowBook(const User &user, const std::string isbn);
    void returnBook(const Borrowing &to_return);

private:
    dbConnection connection;
};

#endif // USERDB_H
