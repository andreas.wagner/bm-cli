#include "dbconnection.h"
#include <limits>
#include <stdexcept>
#include <cstring>
#include <thread>
#include <chrono>
#include <vector>
//#include <stdlib.h>

Blob::Blob(const void *ptr, const size_t size) :
    size(size)
{
    this->ptr = new char[size]();
    for(size_t i = 0; i < size; i++)
    {
        ((char*) this->ptr)[i] = ((char*)ptr)[i];
    }
}

Blob::Blob(const Blob &blob) :
    size(blob.size)
{
    this->ptr = new char[blob.size]();
    for(size_t i = 0; i < size; i++)
    {
        (this->ptr)[i] = (blob.ptr)[i];
    }
}

char *Blob::getPtr() const
{
    return ptr;
}

size_t Blob::getSize() const
{
    return size;
}

Blob::~Blob()
{
    delete[] this->ptr;
}



Statement::Statement(sqlite3_stmt *stmt, const sqlite3 *db) :
    retryCount(8),
    retryDuration(std::chrono::milliseconds(250)),
    stmt(stmt),
    db((sqlite3*) db),
    step_brought_row(false)
{
}

/*
Statement::~Statement()
{
    if(sqlite3_finalize(stmt) != SQLITE_OK)
    {
        throw new std::runtime_error("Statement: finalizing stmt failed.");
    }
}
*/

void Statement::finalize()
{
    if(sqlite3_finalize(stmt) != SQLITE_OK)
    {
        throw std::runtime_error("Statement: finalizing stmt failed.");
    }
}

void Statement::bind(int i, const std::string &text)
{
    if(sqlite3_bind_text(stmt, i, text.data(), text.length(), 0) != SQLITE_OK)
    {
        throw std::runtime_error("Statement: Binding text failed.");
    }
}

void Statement::bind(int i, const Blob &blob)
{
    if(sqlite3_bind_blob(stmt, i, (void*) blob.getPtr(), blob.getSize(), 0) != SQLITE_OK)
    {
        throw std::runtime_error("Statement: Binding Blob failed!");
    }
}

void Statement::bind(int i, const int64_t &j)
{
    int result = 0;
    if((result = sqlite3_bind_int64(stmt, i, (sqlite_int64) j)) != SQLITE_OK)
    {
        throw std::runtime_error("Statement: Binding Blob failed!");
    }
}

bool Statement::step()
{
    char *err_msg = nullptr;
    int retry_count = retryCount;
    step_brought_row = false;
    bool retry = false;
    do
    {
        retry = false;
        int step_ret_val = sqlite3_step(stmt);
        switch(step_ret_val)
        {
        case SQLITE_BUSY:
            std::this_thread::sleep_for(retryDuration);
            if (--retry_count > 0)
            {
                retry = true;;
            }
            else
            {
                throw std::runtime_error("Statement: Database stays busy.");
            }
            break;
        case SQLITE_DONE:
            break;
        case SQLITE_ROW:
            step_brought_row = true;
            break;
        case SQLITE_ERROR:
            throw std::runtime_error("Statement: Error while evaluating step of 'create table'!");
            break;
        case SQLITE_MISUSE:
            throw std::logic_error("Statement: sqlite3_step() returned SQLITE_MISUSE.");
            break;
        default:
            err_msg = (char*) sqlite3_errmsg(db);
            throw std::logic_error(std::string("Statement: sqlite3_step() returned something which goes to the default. ") + err_msg);
            break;
        }
    }
    while(retry);
    return step_brought_row;
}

void Statement::setRetryCount(int n)
{
    this->retryCount = n;
}

void Statement::setRetryDuration(std::chrono::milliseconds& sleep_duration)
{
    this->retryDuration = sleep_duration;
}

Blob Statement::getBlob(int col)
{
    if(step_brought_row)
    {
        void* ret = (void*) sqlite3_column_blob(stmt, col);
        int size = sqlite3_column_bytes(stmt, col);
        Blob retVal(ret, size);
        return retVal;
    }
    else
    {
        throw std::logic_error("Statement: step() not called or no row fetched!");
    }
}

std::string Statement::getText(int col)
{
    if(step_brought_row)
    {
        char* ret = (char*) sqlite3_column_text(stmt, col);
        int size = sqlite3_column_bytes(stmt, col);
        std::string retVal(ret, size);
        return retVal;
    }
    else
    {
        throw std::logic_error("Statement: step() not called or no row fetched!");
    }
}

int64_t Statement::getInt64(int col)
{
    if(step_brought_row)
    {
        int64_t ret =  sqlite3_column_int64(stmt, col);
        return ret;
    }
    else
    {
        throw std::logic_error("Statement: step() not called or no row fetched!");
    }
}

dbConnection::dbConnection() :
    connection(nullptr),
    isOpen(false)
{
}

dbConnection::~dbConnection()
{
    this->close();
}

void dbConnection::open(const std::string &filename)
{
    if(isOpen)
    {
        this->close();
    }
    if(!isOpen)
    {
        if(sqlite3_open(std::string("file:" + filename).c_str(), &connection) != SQLITE_OK)
        {
            throw std::runtime_error("dbConnection: Error when opening database file: " + filename);
        }
        isOpen = true;
    }
}

void dbConnection::close()
{
    if(isOpen)
    {
        if(connection != nullptr)
        {
            if(int result = sqlite3_close_v2(connection) != SQLITE_OK)
            {
                throw std::runtime_error("dbConnection: Error while closing database.");
            }
        }
        isOpen = false;
    }
}

bool dbConnection::getIsOpen()
{
    return isOpen;
}

Statement dbConnection::prepareStatement(const std::string &sql) const
{
    sqlite3_stmt *stmt = nullptr;
    int result;
    char *data = (char*) sql.c_str();
    size_t length = sql.length();
    if((result = sqlite3_prepare_v2(connection, data, length, &stmt, nullptr)) != SQLITE_OK)
    {
        throw std::runtime_error("dbConnection: Preparing failed!");
    }
    Statement ret(stmt, connection);
    return ret;
}
