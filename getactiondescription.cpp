#include <ctime>

#include "getactiondescription.h"
#include "utc_time.h"

std::string getActionDescription::borrowBook(const User &user, const Book &book)
{
    std::string text("Ausleihe:\n");
    text += user.firstname + " " + user.lastname + "\n";
    text += u8"»" + book.title+ u8"« von " + book.authors + "\nISBN: " + book.ISBN + "\n";
    return text;
}

std::string getActionDescription::returnBook(const Borrowing &b, const BookCache &book_cache, const userDB &userdb, const std::string &password, int borrowingduration, int extraPeriodDuration)
{
    std::string text("Rückgabe:\n");
    User user(userdb.getUserByBarcode(b.user_barcode,
                                       password));
    Book book(book_cache.getBookByISBN(b.isbn));
    text += user.firstname + " " + user.lastname + "\n";
    text += u8"»" + book.title+ u8"« von " + book.authors + "\n";
    text += "ISBN: " + book.ISBN + "\n";
    text += "seit " + get_localtime_from_utc(b.date_time_utc_posix);
    text += " Verlängerungen: " + std::to_string(b.extra_periods) + "\n";
    time_t due = b.date_time_utc_posix + borrowingduration * 60 * 60 + b.extra_periods * extraPeriodDuration * 60 * 60;
    text += "Fällig am: " + get_localtime_from_utc(due);
    return text;
}

std::string getActionDescription::incrementExtraPeriods(const bool add_1_to_extraPeriods, const Borrowing &b, const BookCache &book_cache, const userDB &userdb, const std::string &password, int borrowingduration, int extraPeriodDuration)
{
    std::string text("Fristverlängerung:\n");
    User user(userdb.getUserByBarcode(b.user_barcode,
                                       password));
    Book book(book_cache.getBookByISBN(b.isbn));
    text += user.firstname + " " + user.lastname + "\n";
    text += u8"»" + book.title+ u8"« von " + book.authors + "\n";
    text += "ISBN: " + book.ISBN + "\n";
    text += "seit " + get_localtime_from_utc(b.date_time_utc_posix) +"\n";
    text += "Verlängerungen: " + std::to_string(b.extra_periods+1)+ "\n";
    time_t due = b.date_time_utc_posix + borrowingduration * 60 * 60 + (b.extra_periods+(add_1_to_extraPeriods?1:0)) * extraPeriodDuration * 60 * 60;
    text += "Fällig am: " + get_localtime_from_utc(due);
    return text;
}
