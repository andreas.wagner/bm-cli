#include "managebooksdialog.h"
#include "ui_manageBooksDialog.h"
#include "bookcache.h"
#include "lookupbook.h"
#include <string>
#include <memory>


void ManageBooksDialog::on_ISBNLineEdit_editingFinished()
{
    std::string toFind = std::string(ui->ISBNLineEdit->text().toUtf8().constData());
    if(toFind.length() == 10 || toFind.length() == 13)
    {
        Book b = book_cache->getBookByISBN(toFind);
        if(b.title == "" && b.authors == "")
        {
            // TODO: Fehlerbehandlung
            Book tmp = lookupBook::lookupBook(toFind, (*config).at("AccessToken"));
            if(tmp.ISBN != "")
            {
                book_cache->updateBook(tmp);
                ui->AuthorLineEdit->setText(tmp.authors.c_str());
                ui->TitleLineEdit->setText(tmp.title.c_str());
                book_cache->updateBook(tmp);
            }
            else
            {
                ui->AuthorLineEdit->setText("");
                ui->TitleLineEdit->setText("");
            }
        }
        else {
            ui->AuthorLineEdit->setText(b.authors.c_str());
            ui->TitleLineEdit->setText(b.title.c_str());
        }
    }
    else
    {
        ui->AuthorLineEdit->setText("");
        ui->TitleLineEdit->setText("");
    }
}


void ManageBooksDialog::on_applyPushButton_clicked()
{
    std::string toFind = std::string(ui->ISBNLineEdit->text().toUtf8().constData());
    if(toFind.length() == 10 || toFind.length() == 13)
    {
        Book b;
        b.ISBN = toFind;
        b.authors = ui->AuthorLineEdit->text().toUtf8().constData();
        b.title = ui->TitleLineEdit->text().toUtf8().constData();
        book_cache->updateBook(b);
    }
    ui->ISBNLineEdit->setText("");
    ui->AuthorLineEdit->setText("");
    ui->TitleLineEdit->setText("");
}
