#ifndef MANAGEBOOKSDIALOG_H
#define MANAGEBOOKSDIALOG_H

#include <QWidget>
#include <QDialog>
#include <memory>
#include "bookcache.h"


class ManageBooksDialog
{
protected:
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void on_ISBNLineEdit_editingFinished();

    void on_applyPushButton_clicked();

private:
    std::shared_ptr<BookCache> book_cache;
    std::shared_ptr<std::map<std::string, std::string>> config;
};

#endif // MANAGEBOOKSDIALOG_H
