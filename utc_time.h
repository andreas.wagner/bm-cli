#include <ctime>
#include <string>

time_t get_current_utc();

std::string get_localtime_from_utc(const time_t &time);
