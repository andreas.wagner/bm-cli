#ifndef BOOKCACHE_H
#define BOOKCACHE_H

#include <sqlite3.h>
#include <string>
#include <list>
#include <iostream>
#include "dbconnection.h"

struct Book
{
    std::string ISBN;
    std::string authors;
    std::string title;
};

class BookCache
{
public:
    BookCache();
    ~BookCache();

    void open(const std::string &filename);
    bool isValid();
    void close();
    void initializeDatabase();
    Book getBookByISBN(const std::string &isbn) const;
    void updateBook(const Book &book) const;
    std::list<Book> getAllBooks();

private:
    dbConnection connection;
};


#endif // BOOKCACHE_H
