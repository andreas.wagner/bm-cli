#ifndef PRINTUSERBARCODESDIALOG_H
#define PRINTUSERBARCODESDIALOG_H

#include <memory>

#include "userdb.h"

class PrintUserBarcodesDialog
{

private slots:
    void on_fontPushButton_clicked();

    void on_printerPushButton_clicked();

    bool eventFilter(QObject *obj, QEvent *event);

    void on_FilterLineEdit_editingFinished();

    void on_printSelectedUsersPushButton_clicked();

    void on_selectAllPushButton_clicked();

    void on_pushButton_clicked();

    void on_logoLineEdit_textChanged(const QString &arg1);

private:
    std::shared_ptr<userDB> userdb;
    std::string password;
    std::shared_ptr<std::map<std::string, std::string>> config;
};

#endif // PRINTUSERBARCODESDIALOG_H
