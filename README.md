Borrowing-Manager v2 is a small program for libraries which only need management
of users and borrowed books. It can use access to the German National Librarys
SRU-interface to gather information about a book via its ISBN.

UPDATE: You can leave the field "Access Token" empty.

Users are able to borrow books using theier name and/or a tag (like their class)
or using their library-id using a barcode. Identity-Cards with barcodes are
printable using this program.

Does not contain address-management.

WITHOUT ANY WARRANTY.
