#include "utc_time.h"

#include <ctime>
#include <string>

time_t get_current_utc()
{
    time_t rawtime;
    time(&rawtime);
    return rawtime;
}


std::string get_localtime_from_utc(const time_t &time)
{
    struct tm* timeStruct = std::localtime(&time);
    char buf[1024];
    std::strftime(buf, 1024, "%Y-%m-%d %H:%M:%S", timeStruct);
    std::string retVal(buf);
    return retVal;
}
