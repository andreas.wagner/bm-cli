#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <string>
#include <sqlite3.h>
#include <chrono>
#include <vector>

class dbConnection;

class Blob
{
public:
    Blob(const void *ptr, const size_t size);
    Blob(const Blob &blob);
    ~Blob();

    char* getPtr() const;
    size_t getSize() const;

private:
    char *ptr;
    size_t size;
};

class Statement
{
public:
    Statement(sqlite3_stmt *stmt, const sqlite3 *db);
//    ~Statement();

//    Statement(Statement &s) = delete;
//    Statement operator=(Statement &s) = delete;

    void bind(int i, const std::string &text);
    void bind(int i, const Blob &blob);
    void bind(int i, const int64_t &j);

    bool step();

    void finalize();

    Blob getBlob(int col);
    std::string getText(int col);
    int64_t getInt64(int col);

    void setRetryCount(int n);
    void setRetryDuration(std::chrono::milliseconds& sleep_duration);

private:
    int retryCount;
    std::chrono::milliseconds retryDuration;
    sqlite3_stmt *stmt;
    sqlite3 *db;
    bool step_brought_row;
};

class dbConnection
{
public:
    dbConnection();
    ~dbConnection();
    void open(const std::string &filename);
    void close();
    bool getIsOpen();
    Statement prepareStatement(const std::string &sql) const;

    //friend Statement::Statement(sqlite3_stmt *stmt, const dbConnection &db);

private:
    sqlite3 *connection;
    bool isOpen;
};

#endif // DBCONNECTION_H
