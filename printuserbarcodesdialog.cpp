#include <memory>
#include <vector>

#include <iostream>

#include "printuserbarcodesdialog.h"
#include "ui_printUserBarcodes.h"
#include "userdb.h"

void drawBarcode(const QPointF &point, const qreal &height, const qreal &scale, QPainter &painter, const std::string &barcode);
std::vector<uint8_t> get_2of5_barcode(const std::string &s);
std::vector<uint8_t> get_interleaved_2of5_barcode(const std::string &s);


PrintUserBarcodesDialog::PrintUserBarcodesDialog(QWidget *parent, std::shared_ptr<std::map<std::string, std::string>> &config, const std::shared_ptr<userDB> &userdb, const std::string &password) :
    userdb(userdb),
    password(password),
    config(config)
{
    if(config->count("Logo") != 0)
    {
        ui->logoLineEdit->setText((*config)["Logo"].c_str());
    }
    if(config->count("Font") != 0)
    {
        selectedFont.fromString((*config)["Font"].c_str());
        QString fontName(selectedFont.toString().split(",")[0]);
        ui->selectedFontLabel->setText(fontName);
    }
}

void PrintUserBarcodesDialog::on_fontPushButton_clicked()
{
    QFontDialog fontDialog(selectedFont, this);
    fontDialog.exec();
    selectedFont = fontDialog.currentFont();
    (*config)["Font"] = selectedFont.toString().toUtf8().constData();
    QString fontName(selectedFont.toString().split(",")[0]);
    ui->selectedFontLabel->setText(fontName);
}

void PrintUserBarcodesDialog::on_printerPushButton_clicked()
{
    QPrintDialog printDialog(&printer, this);
    int printDialogReturnValue = printDialog.exec();
    if(printDialogReturnValue == QDialog::Accepted)
    {
        if(printDialog.printer()->printerName().size() == 0)
        {
            ui->selectedPrinterLabel->setText(printDialog.printer()->outputFileName());
        }
        else
        {
            ui->selectedPrinterLabel->setText(printDialog.printer()->printerName());
        }
    }
}


void PrintUserBarcodesDialog::on_FilterLineEdit_editingFinished()
{
    for(QTableWidgetItem *item : tableItemList)
    {
        delete item;
    }
    tableItemList.clear();

    std::list<User> userList;

    if(ui->FilterLineEdit->text().size() == 8)
    {
        bool isNumeric = true;
        std::string s = ui->FilterLineEdit->text().toUtf8().constData();
        for(int i = 0; i < s.size(); i++)
        {
            if(!std::isdigit(s[i]))
            {
                isNumeric = false;
                break;
            }
        }
        if(isNumeric)
        {
            userList.push_back(userdb->getUserByBarcode(s, password));
        }
    }
    else
    {
        userList = userdb->getUsersForSearchstring(
                                     password,
                                     ui->FilterLineEdit->text().toUtf8().constData());
    }

    ui->userTableWidget->clear();
    ui->userTableWidget->setColumnCount(3);
    QStringList horizontalHeaders{
        "Vorname",
        "Nachname",
        "Barcode"
    };
    ui->userTableWidget->setHorizontalHeaderLabels(horizontalHeaders);
    ui->userTableWidget->setRowCount(userList.size());
    int rowCount = 0;
    for(User u : userList)
    {
        QTableWidgetItem *item1 = new QTableWidgetItem(u.firstname.c_str());
        item1->setFlags(item1->flags() & ~Qt::ItemIsEditable);
        QTableWidgetItem *item2 = new QTableWidgetItem(u.lastname.c_str());
        item2->setFlags(item2->flags() & ~Qt::ItemIsEditable);
        QTableWidgetItem *item3 = new QTableWidgetItem(u.barcode.c_str());
        item3->setFlags(item3->flags() & ~Qt::ItemIsEditable);

        ui->userTableWidget->setItem(rowCount, 0, item1);
        ui->userTableWidget->setItem(rowCount, 1, item2);
        ui->userTableWidget->setItem(rowCount, 2, item3);

        tableItemList.push_back(item1);
        tableItemList.push_back(item2);
        tableItemList.push_back(item3);
        rowCount++;
    }
}

void PrintUserBarcodesDialog::on_printSelectedUsersPushButton_clicked()
{
    std::list<User> userList;
    QList<QTableWidgetItem *> items(ui->userTableWidget->selectedItems());
    for(QTableWidgetItem *item : items)
    {
        if(item->column() == 2)
        {
            userList.push_back(userdb->getUserByBarcode(item->text().toUtf8().constData(), password));
        }
    }

    QPainter painter;
    printer.setFontEmbeddingEnabled(true);
    printer.setDocName(ui->FilterLineEdit->text());
    printer.pageRect().setHeight(printer.paperRect().height()-80);
    printer.pageRect().setY(40);
    painter.begin(&printer);
    painter.setFont(selectedFont);

    QPointF current;

    current.setX(printer.pageRect().x());
    current.setY(printer.pageRect().y());
    for(User u : userList)
    {
        QRectF clip;
        clip.setTopLeft(current);
        clip.setHeight(printer.pageRect().height()/7.0);
        clip.setWidth(printer.pageRect().width()/2.0);
        painter.setClipRect(clip);

        std::list<std::string> tagList = userdb->getTagsForUser(u);
        std::string tags;
        int numberOfTags = tags.size();
        for(std::string t : tagList)
        {
            tags += t;
            numberOfTags--;
            if(numberOfTags >0)
                tags += " ";
        }

        QPointF derived = current;

        derived.setX(derived.x()+0.1);
        derived.setY(derived.y()+selectedFont.pointSizeF()+0.1);
        drawBarcode(derived, 40, 1, painter, u.barcode);

        derived.setY(derived.y() + printer.pageRect().height()/7.0 - (2.0*selectedFont.pointSizeF()*1.2) - 20);
        painter.drawText(derived, std::string(u.firstname + " " + u.lastname).c_str());
        derived.setY(derived.y() + selectedFont.pointSizeF()*1.2);
        painter.drawText(derived, tags.c_str());

        // paint logo
        if(!(ui->logoLineEdit->text().size() == 0))
        {
            derived = current;
            derived.setX(derived.x() + 90);
            QSvgRenderer renderer(ui->logoLineEdit->text());
            clip.setTopLeft({0, 0});
            clip.setWidth(250.0);
            clip.setHeight(100.0);
            painter.translate(derived);
            if(renderer.isValid() && painter.isActive())
            {
                painter.setClipRect(clip);
                QRectF viewBox(0.0, 0.0, 250.0, 350.0);
                renderer.setViewBox(viewBox);
                renderer.render(&painter);
            }
            QPointF minus_derived;
            minus_derived.setX(-derived.x());
            minus_derived.setY(-derived.y());
            painter.translate(minus_derived);
        }

        if((current.x() > printer.pageRect().x()-0.00000001) && (current.x() < printer.pageRect().x()+0.000000001))
            current.setX(printer.pageRect().x() + printer.pageRect().width()/2.0);
        else
        {
            current.setX(printer.pageRect().x());
            current.setY(current.y() + printer.pageRect().height()/7.0);
        }
        if(current.y() >= printer.pageRect().y() + printer.pageRect().height()-8)
        {
            current.setY(printer.pageRect().y());
            printer.newPage();
        }
    }
    painter.end();
}

void PrintUserBarcodesDialog::on_selectAllPushButton_clicked()
{
    ui->userTableWidget->selectAll();
}

void drawBarcode(const QPointF &point, const qreal &height, const qreal &scale, QPainter &painter, const std::string &barcode)
{
    std::vector<uint8_t> data(get_interleaved_2of5_barcode(barcode));
    QRectF rect;
    rect.setTopLeft(point);
    rect.setHeight(height);
    QBrush filled(QColor(0, 0, 0, 255));

    bool latch_is_up = true;
    for(int i = 0; i < data.size(); i++)
    {
        if(latch_is_up)
        {
            rect.setWidth(scale * data[i]);
            painter.fillRect(rect, filled);
            rect.translate(rect.width(), 0);
        }
        else
        {
            rect.translate(scale*data[i], 0);
        }
        latch_is_up = !latch_is_up;
    }

}

std::vector<uint8_t> get_matrix_2of5_barcode(const std::string &s)
{
    std::vector<uint8_t> returnValue;
    uint8_t factors[10][6] = {
        {1, 1, 3, 3, 1, 1}, // 113311
        {3, 1, 1, 1, 3, 1}, // 311131
        {1, 3, 1, 1, 3, 1}, // 131131
        {3, 3, 1, 1, 1, 1}, // 331111
        {1, 1, 3, 1, 3, 1}, // 113131
        {3, 1, 3, 1, 1, 1}, // 313111
        {1, 3, 3, 1, 1, 1}, // 133111
        {1, 1, 1, 3, 3, 1}, // 111331
        {3, 1, 1, 3, 1, 1}, // 311311
        {1, 3, 1, 3, 1, 1}  // 131311
    };
    uint8_t start[] = {4, 1, 1, 1, 1, 1};
    uint8_t end[] = {4, 1, 1, 1, 1};
    returnValue.reserve(
            sizeof(start)
            + sizeof(factors[0]) * s.size()
            + sizeof(end)
            );
    for(uint8_t a : start)
    {
        returnValue.push_back(a);
    }
    for(auto c : s)
    {
        for(uint8_t a : factors[c-'0'])
        {
            returnValue.push_back(a);
        }
    }
    for(uint8_t a : end)
    {
        returnValue.push_back(a);
    }
    return returnValue;
};


std::vector<uint8_t> get_interleaved_2of5_barcode(const std::string &s)
{
    std::vector<uint8_t> returnValue;
    uint8_t factors[10][5] = {
    {1, 1, 3, 3, 1},// "11331",
    {3, 1, 1, 1, 3},// "31113",
    {1, 3, 1, 1, 3},// "13113",
    {3, 3, 1, 1, 1},// "33111",
    {1, 1, 3, 1, 3},// "11313",
    {3, 1, 3, 1, 1},// "31311",
    {1, 3, 3, 1, 1},// "13311",
    {1, 1, 1, 3, 3},// "11133",
    {3, 1, 1, 3, 1},// "31131",
    {1, 3, 1, 3, 1} // "13131"
    };
    uint8_t start[] = {1, 1, 1, 1};
    uint8_t end[] = {3, 1, 1};
    std::string copy(s);
    if(s.size() & 1) // is it odd?
    {
        copy = "0" + copy;
    }
    returnValue.reserve(
                sizeof(start)
                + sizeof(factors[0]) * copy.size()
                + sizeof(end)
                );
    for(uint8_t a : start)
    {
        returnValue.push_back(a);
    }
    for(int i = 0; i < copy.size(); i += 2)
    {
        uint8_t mixed[10];
        uint8_t bars[5];
        uint8_t spaces[5];
        for(int j = 0; j < 5; j++)
        {
            bars[j] = factors[copy[i]-'0'][j];
        }
        for(int j = 0; j < 5; j++)
        {
            spaces[j] = factors[copy[i+1]-'0'][j];
        }
        int k = 0;
        for(int j = 0; j < 5; j++)
        {
            mixed[k] = bars[j];
            k++;
            mixed[k] = spaces[j];
            k++;
        }
        for(uint8_t a : mixed)
        {
            returnValue.push_back(a);
        }
    }
    for(uint8_t a : end)
    {
        returnValue.push_back(a);
    }
    return returnValue;
}

void PrintUserBarcodesDialog::on_pushButton_clicked()
{
    QFileDialog dialog(this, "Logo auswählen", "", ".svg-Datei (*.svg);; Alle Dateien (*)");
    if(dialog.exec() == QDialog::Accepted)
    {
        QStringList list(dialog.selectedFiles());
        ui->logoLineEdit->setText(list[0]);
    }
}

void PrintUserBarcodesDialog::on_logoLineEdit_textChanged(const QString &arg1)
{
    (*config)["Logo"] = arg1.toUtf8().constData();
}
