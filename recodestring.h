#ifndef RECODESTRING_H
#define RECODESTRING_H

#include <string>
#ifndef _my_RECODE_H_
#define _my_RECODE_H_
#include <recode.h>
#endif

namespace recodeString
{
    void initialize();
    void finalize();
    std::string toUtf8(const std::string &encoding, const std::string &str);

    static RECODE_OUTER outer;
};

#endif // RECODESTRING_H
