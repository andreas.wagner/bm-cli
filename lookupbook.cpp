#include "lookupbook.h"
#include <rapidxml/rapidxml.hpp>
#include <cstring>
#include <string>
#include <iostream>

#include "urlfetch.h"

Book lookupBook::lookupBook(const std::string &isbn,  const std::string &token)
{
    Book returnValue;
    returnValue.ISBN = "";
    returnValue.title = "";
    returnValue.authors = "";

    std::string search_isbn(isbn);
    std::string url("https://services.dnb.de/sru/accessToken~" + token + "/dnb?version=1.1&operation=searchRetrieve&query=isbn%3d" + search_isbn + "&recordSchema=MARC21-xml");

    std::string xml(URLFetch::fetchHTTPToString(url));
    char * xml_copy = new char[xml.size()+1];
    strcpy(xml_copy, xml.c_str());

    rapidxml::xml_document<> doc;
    try
    {
        doc.parse<0>(xml_copy);
    }
    catch(std::exception e)
    {
        std::cout << e.what() << std::endl;
        std::cout << xml;
    }

    rapidxml::xml_node<> *record = doc.first_node("searchRetrieveResponse");
    if(record == nullptr)
        return returnValue;
    record = record->first_node("records");
    if(record == nullptr)
        return returnValue;
    record = record->first_node("record");


    while(record != nullptr)
    {
        rapidxml::xml_node<> *node = record->first_node("recordData");
        if(node == nullptr)
            break;
        node = node->first_node("record");
        if(node == nullptr)
            break;
        node = node->first_node("datafield");

        bool isRelevant = false;


        while (node != nullptr)
        {
            rapidxml::xml_attribute<> *attrib = node->first_attribute("tag");

            while (attrib != nullptr)
            {
                if(strncmp(attrib->value(), "020", 3) == 0)
                {
                    rapidxml::xml_node<> *isbn_node = node->first_node("subfield");
                    while(isbn_node != nullptr)
                    {
                        rapidxml::xml_attribute<> *isbn_attrib = isbn_node->first_attribute("code");
                        while(isbn_attrib != nullptr)
                        {
                            if((strncmp(isbn_attrib->value(), "a", 1) == 0)||(strncmp(isbn_attrib->value(), "z", 1) == 0))
                            {
                                if(strncmp(search_isbn.c_str(), isbn_node->value(), search_isbn.size()) == 0)
                                {
                                    returnValue.ISBN = isbn_node->value();
                                    isRelevant = true;
                                }
                            }
                            isbn_attrib = isbn_attrib->next_attribute("code");
                        }
                        isbn_node = isbn_node->next_sibling("subfield");
                    }
                }
                attrib = attrib->next_attribute("tag");
            }
            node = node->next_sibling("datafield");
        }

        if(isRelevant)
        {
            node = record->first_node("recordData");
            if(node == nullptr)
                break;
            node = node->first_node("record");
            if(node == nullptr)
                break;
            node = node->first_node("datafield");

            while (node != nullptr)
            {
                rapidxml::xml_attribute<> *attrib = node->first_attribute("tag");

                while (attrib != nullptr)
                {
                    if(strncmp(attrib->value(), "245", 3) == 0)
                    {
                        rapidxml::xml_node<> *local_node = node->first_node("subfield");
                        while(local_node != nullptr)
                        {
                            rapidxml::xml_attribute<> *local_attrib = local_node->first_attribute("code");
                            while(local_attrib != nullptr)
                            {
                                if(strncmp(local_attrib->value(), "a", 1) == 0)
                                {
                                    returnValue.title = local_node->value();
                                    std::cerr << local_node->value() << returnValue.title << std::endl;
                                }
                                if(strncmp(local_attrib->value(), "c", 1) == 0)
                                {
                                    returnValue.authors = local_node->value();
                                }
                                local_attrib = local_attrib->next_attribute("code");
                            }
                            local_node = local_node->next_sibling("subfield");
                        }
                    }
                    attrib = attrib->next_attribute("tag");
                }
                node = node->next_sibling("datafield");
            }
        }

        record = record->next_sibling("record");
    }

    delete[] xml_copy;

    return returnValue;
}
