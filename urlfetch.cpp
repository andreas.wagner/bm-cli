#include "urlfetch.h"

/*#include <stdexcept>

#include <iostream>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/select.h>
*/

#include <string>
//#include <iostream>

#include <curl/curl.h>

std::string returnvalue;

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
	returnvalue += std::string((char *)buffer, size * nmemb);
	return size * nmemb;
}


std::string URLFetch::fetchHTTPToString(const std::string &url)
{
	returnvalue = "";
	CURL *handle = curl_easy_init();
	if(handle)
	{
		curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
		curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
		CURLcode success = curl_easy_perform(handle);	
		curl_easy_cleanup(handle);
	}
	//std::cout << returnvalue << std::endl;
	return returnvalue;
}

/*std::string URLFetch::fetchHTTPToString(const std::string &url)
{
    struct addrinfo hints;
    struct addrinfo *result, *result_iterator;
    std::string domain_name;
    std::string port("80");
    std::string path;

    int socket_fd = -1;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;
    hints.ai_addr = nullptr;
    hints.ai_next = nullptr;
    hints.ai_addrlen = 0;
    hints.ai_canonname = nullptr;

    // extract domain_name, service and path from url
    {
        size_t pos_protocol = url.find("://");
        size_t pos_domain_name = url.find("/", pos_protocol+3);
        size_t pos_port = url.find(":", pos_protocol+3, pos_domain_name-pos_protocol);

        if(pos_protocol == std::string::npos)
        {
            throw new std::runtime_error("URLFetch::fetchHTTPToString: 'http://' not found in string "
                                         + url);
        }

        if(pos_domain_name == std::string::npos)
        {
            pos_domain_name = url.length();
        }

        if(pos_port != std::string::npos)
        {
            domain_name = url.substr(pos_protocol+3, pos_port-pos_protocol-3);
            port = url.substr(pos_port+1, pos_domain_name-pos_port-1);
        }
        else
        {
            domain_name = url.substr(pos_protocol+3, pos_domain_name-pos_protocol-3);
        }

        if(pos_domain_name != url.length())
        {
            path = url.substr(pos_domain_name);
        }
        else {
            path = "/";
        }
    }

    // domainname-resolution
    {
        int s = getaddrinfo(domain_name.c_str(), port.c_str(), &hints, &result);
        if (s != 0)
        {
            throw new std::runtime_error(std::string("URLFetch::fetchToString(): getaddrinfo: ")
                                         + std::string(gai_strerror(s)));
        }
    }
    for(result_iterator = result;
        result_iterator != nullptr;
        result_iterator = result_iterator->ai_next)
    {
        socket_fd = socket(result_iterator->ai_family,
                           result_iterator->ai_socktype,
                           result_iterator->ai_protocol);
        if(socket_fd == -1)
            continue;
        if(connect(socket_fd,
                   result_iterator->ai_addr,
                   result_iterator->ai_addrlen)
                != -1)
            break; // if connect succeeded
        close(socket_fd);
    }

    if(result_iterator == nullptr)
    {
        freeaddrinfo(result);
        throw new std::runtime_error("URLFetch::fetchToString(): Could not connect.");
    }
    freeaddrinfo(result);

    // we should have a working socket_fd right now.


    std::string http_request = "GET " + path + " HTTP/1.1\r\nHost: " + domain_name
            + "\r\nConnection: close\r\n\r\n";

    for (size_t i = 0; i < http_request.length();)
    {
        std::string to_send = http_request.substr(i, http_request.length()-i);
        int written = write(socket_fd, (void *) (to_send.c_str()), to_send.length());
        i += (size_t) written;
    }

    std::string http_reply;
    bool readAnother = true;

    do
    {
        fd_set read_fds;
        struct timeval timeout;
        int retVal;

        FD_ZERO(&read_fds);
        FD_SET(socket_fd, &read_fds);

        timeout.tv_sec = 2;
        timeout.tv_usec = 50000;

        retVal = select(socket_fd+1, &read_fds, nullptr, nullptr, &timeout);
        if(retVal != -1 && retVal != 0)
        {
            const int bufSize = 1600;
            void* buf = new uint8_t[bufSize](); // TODO: find out whether that's portable.
            int bytes_read = read(socket_fd, buf, bufSize);

            if(bytes_read < 1)
            {
                readAnother = false;
            }
            else
            {
                http_reply += std::string((char*) buf, (size_t) bytes_read);
                readAnother = true;
            }
        }
        else
        {
            readAnother = false;
        }
    }
    while(readAnother);

    close(socket_fd);

    size_t pos = http_reply.find("\r\n\r\n");

    if(http_reply.find("Transfer-Encoding: chunked", 0, pos) != std::string::npos)
    {
        // start dechunking
        std::string chunked_response;
        std::string response;
        std::stringstream conv;
        int skip = 1;

        if(pos == std::string::npos)
        {
            return "";
        }
        else
        {
            chunked_response = http_reply.substr(pos+4);
        }

        while(skip > 0)
        {
            int number_length = chunked_response.find("\r\n")+2;
            if(number_length == std::string::npos)
            {
                throw new std::runtime_error("Decoding chunking failed!");
            }
            conv << std::hex << chunked_response.substr(0, number_length);
            conv >> skip;

            response += chunked_response.substr(number_length, skip);
            chunked_response = chunked_response.substr(skip+number_length+2); // +2 because of /r/n at the end
        }
        return response;
    }
    else
    {
        return http_reply.substr(pos);
    }
}*/

